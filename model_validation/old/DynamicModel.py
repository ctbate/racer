import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

class DynamicModelBicycle:
    def __init__(self):
        # state
        self.yaw = 0.0
        self.dyaw = 0.0
        self.ddyaw = 0.0
        self.x = np.zeros(2)   
        self.v = np.zeros(2)   
        self.a = np.zeros(2)           
        self.B = 0.0 # slip
        self.dB = 0.0  # slip rate        

        # Forces (intermediate variables or inputs)
        self.Fxr = 0.0 # rear tire force
        self.Fmax = 10
        self.Fyf = 0.0
        self.Fyr = 0.0
        self.slipFront = 0.0
        self.slipRear = 0.0
        
        # Configuration
        self.Lr = 0.08 # dist from COG to rear wheel 
        self.Lf = 0.30-self.Lr 
        self.Iz = 0.24 # Normal inertial
        self.m = 2.0 # Mass

        # inputs
        self.steering = 0.0 # 0 to 1
        self.throttle = 0.0 # 0 to 1

        # sensors
        self.v_est = 0.0

        # Tire coefficients
        self.B = 7.4        
        self.C = 1.2
        self.D = -2.27

    def Reset(self):
        self.yaw = 0.0
        self.dyaw = 0.0
        self.ddyaw = 0.0
        self.x = np.zeros(2)   
        self.v = np.zeros(2)   
        self.a = np.zeros(2)           
        self.B = 0.0 # slip
        self.dB = 0.0  # slip rate
        self.Fxr = 0.0 # rear tire force
        self.Fmax = 1
        self.Fyf = 0.0
        self.Fyr = 0.0
        self.steering = 0.0 # 0 to 1
        self.throttle = 0.0 # 0 to 1

        
    # For sensor fusion
    def SetAccelIMU( self, x, y,dt):
        self.v_est += x*dt
    
    def Steer(self, angle):
        self.steering = angle*0.5

    def Throttle(self, throttle):
        self.throttle = throttle

    def CalcRearSlip(self):
        return np.arctan2(self.B*self.v[0]+self.dyaw*self.Lf, self.v[0])-self.steering

    def CalcFrontSlip(self):
        return np.arctan2(self.B*self.v[0]-self.dyaw*self.Lr, self.v[0])

    def CalcFrontSlipLinear(self):
        z = self.steering - (self.v[1]+self.Lf*self.dyaw)/self.v[0]
        if(np.isnan(z)):
            return 0.0
        return z

    def CalcRearSlipLinear(self):
        z =  (self.Lr*self.dyaw-self.v[1])/self.v[0]
        if(np.isnan(z)):
            return 0.0
        return z

    def TireForce(self, slip,mu,B,C,Fz,Fymax,acr):
        if np.abs(slip) <= acr:
            return mu*Fz*np.sin(C*np.arctan(B*slip))
        else:
            return Fymax*np.sign(slip)      

    def TireForceLinear(self, slip, isRear=False):
        Car = 1.76
        Caf = 1.76
        C = Car if isRear else Caf
        z = C*slip
        if(np.isnan(z)):
            return 0.0
        return z

    def TireForcePlot(self):
        slipAngle = np.linspace(-0.05,0.05,50)
        mu = 0.2
        Fz = 2.6*9.8/4
        acr =0.5
        Fx = 1.0
        Fymax = np.sqrt( (mu*Fz)**2-Fx**2 )
        acr = np.tan( np.arcsin( np.sqrt(Fz**2-Fx**2)/(mu*Fz) )/self.C )/self.B
        result = np.zeros_like(slipAngle) 
        result2 = np.zeros_like(slipAngle)
        for i in range(len(slipAngle)):
            result[i] = self.TireForce( slipAngle[i], mu, self.B, self.C, Fz, Fymax, acr )
            result2[i] = self.TireForceLinear(slipAngle[i])

        plt.figure()
        plt.plot(slipAngle, result)
        plt.plot(slipAngle, result2)
        plt.show()

    def Step(self, dt):                         
        self.x[0] += (self.v[0]*np.cos(self.yaw)-self.v[1]*np.sin(self.yaw))*dt
        self.x[1] += (self.v[0]*np.sin(self.yaw)+self.v[1]*np.cos(self.yaw))*dt
        self.yaw  += self.dyaw *dt
        self.v[0] += self.a[0]*dt
        self.v[1] += self.a[1]*dt                        
        self.dyaw += self.ddyaw*dt

        self.slipFront = self.CalcFrontSlipLinear()
        self.slipRear = self.CalcRearSlipLinear()

        self.Fyf = self.TireForceLinear(self.slipFront,False)
        self.Fyr = self.TireForceLinear(self.slipRear,True)
        
        self.Fxr = self.throttle*self.Fmax
        v = np.linalg.norm(self.v)
        self.a[0] = (2/self.m)*(self.Fxr) - (0.2*(v)**2)+self.v[1]*self.dyaw
        self.a[1] = (2/self.m)*(self.Fyr+self.Fyf)-self.v[0]*self.dyaw 
        self.ddyaw = (2/self.Iz)*(self.Lf*self.Fyf-self.Lr*self.Fyr)
        if(np.isnan(self.a[0])):
            self.a[0] = 0.0
        if(np.isnan(self.a[1])):
            self.a[1] = 0.0
        if(np.isnan(self.ddyaw)):
            self.ddyaw = 0.0

    def TestLong(self):
        dt = 0.01
        numStep =500
        self.throttle = 1
        results = np.zeros((numStep,6))
        for i in range(numStep):
            self.Step(dt)
            results[i,0] = self.x[0]
            results[i,1] = self.a[0]
            results[i,2] = self.v[0]    
            results[i,4:6] = np.array([self.slipFront, self.slipRear])

        times=np.linspace(0,numStep*dt,numStep)
        #plt.plot(times,results[:,0], label="Position")
        plt.plot(times,results[:,1],label="Acceleration")
        plt.plot(times,results[:,2], label="Velocity")
        plt.legend()
        plt.xlabel("Seconds")
        plt.title("Longitudinal Model, Max Throttle")
        plt.show()

    def TestLat(self):
        dt = 0.01
        numStep =5000
        times=np.linspace(0,numStep*dt,numStep)
        steering = np.sin(0.5 * np.pi * times)*0.01
        results = np.zeros((numStep,18))        
        for i in range(numStep):
            self.Step(dt)
            self.throttle = 1.0     
            self.steering = steering[i]
            results[i,0:2] = self.x
            results[i,2:4] = self.a
            results[i,10:12] = self.v
            results[i,4:6] = np.array([self.slipFront, self.slipRear])
            results[i,6:8] = np.array([self.Fyf, self.Fyr])
            results[i,8:10] = np.array([self.yaw, self.dyaw])        
            

        plt.figure()
        plt.scatter(results[:,0],results[:,1], s=1)
        plt.title("Predicted Trajectory, Sinosoidal Steering, Const. Throttle")
        plt.show()

        plt.figure()   
        plt.plot(times,results[:,2],label="Long. Acceleration")
        plt.plot(times,results[:,10], label="Long. Velocity")
        plt.legend()
        plt.show()

        plt.figure()   
        plt.plot(times,results[:,3],label="Lateral Acceleration")
        plt.plot(times,results[:,11], label="Lateral Velocity")
        plt.legend()
        plt.show()

        plt.figure()   
        plt.plot(times,results[:,8],label="Yaw Rate")        
        plt.legend()
        plt.show()

        plt.figure()   
        plt.plot(times,results[:,4],label="Slip Front")
        plt.plot(times,results[:,5], label="Slip Rear")
        plt.plot(times,results[:,6],label="Lateral Tire Force Front")
        plt.plot(times,results[:,7], label="Lateral Tire Force Rear")
        plt.legend()
        plt.title("Predicted Tire Slip and Tire Lateral Forces")
        plt.show()        

if __name__=='__main__':
    model = DynamicModelBicycle()

    model.TireForcePlot()
    model.TestLong()
    model.Reset()
    model.TestLat()