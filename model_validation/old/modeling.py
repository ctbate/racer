import numpy as np
import matplotlib.pyplot as plt
import modeling.smoothing

class Model():
    def __init__(self):
        self.GR = 1.0
        self.re = 0.05
        self.Je_recip = 1.0
        self.yaw = 0.0
        self.dyaw = 0.0
        self.ddyaw = 0.0
        self.Fmax = 1
        self.mass = 2
        self.fLoad = 0.0
        self.ww = 0.0
        self.we = 0.0
        self.dwe = 0.0
        self.ca = 1.36
        self.et = 0.0
        self.throttle = 0.0
        self.steering = 0.0
        self.x = np.zeros(2)        
        self.v = np.zeros(2)
        self.a = np.zeros(2)    
        self.cr1 = 0.01
        self.ac = np.array([400, 0.1, -0.002])
        self.Flon = 0.0         
        self.s = 0.0      
        self.Iz = 10.0
        self.v[0] = 0.01
        self.lf=0.15
        self.lr=0.1
        self.Flat=0.0
        self.B = 0.0        

    def Step(self, dt):
        # Integration
        self.x[0] += (self.v[0]*np.cos(self.yaw)-self.v[1]*np.sin(self.yaw))*dt
        self.x[1] += (self.v[0]*np.sin(self.yaw)+self.v[1]*np.cos(self.yaw))*dt
        self.v[0] += self.a[0]*dt
        self.we += self.dwe *dt
        self.yaw += self.dyaw*dt
        self.dyaw += self.ddyaw*dt        

        self.fLoad = self.cr1*self.v[0]+self.ca*self.v[0]**2
        self.et = self.throttle*(self.ac[0]+self.ac[1]*self.we+self.ac[2]*self.we**2)
        self.dwe = self.Je_recip * (self.et - self.GR*self.re*self.fLoad)
        self.ww = self.GR*self.we
        self.s = (self.ww*self.re - self.v[0])/self.v[0]
        if(np.isnan(self.s)):
            self.s = 0.0
        self.Flon = self.s*self.Fmax if abs(self.s) < 1 else self.Fmax

        self.a[0] = (self.Flon - self.fLoad)*(1/self.mass)

        self.Flat = (self.v[1]+self.lf)
        self.a[1] = (1/self.mass)*(self.Flat*np.cos(self.steering)+self.Flat) - (self.v[0] * self.dyaw)
        if(np.isnan(self.a[1])):
            self.a[1] = 0.0
        self.ddyaw = (2/self.Iz)+(2/self.mass)*(self.lf*self.Flat-self.lr*self.Flat)

class HindModel:
    def __init__(self):
        # state
        self.B = 0.0 # Side slip angle
        self.dB = 0.0
        self.Ux = 0.0
        self.dUx = 0.01        
        self.r = 0.0 # Yaw rate
        self.dr = 0.0 
        self.yaw = 0.0

        self.x = np.zeros(2)   

        self.Uy = 0.0
        self.dUy = 0.0

        self.aF = 0.0 # front slip
        self.aR = 0.0 # rear slip

        # inputs
        self.steering = 0.0
        self.throttle = 0.0
        
        self.Fxr = 0.0 # Rear tire force, long
        self.Iz = 500.0
        self.lf = 1.35
        self.lr = 1.15

        self.Fyf = 0.0 # Front  tire force, lateral
        self.Fyr = 0.0 # Rear tire force, lateral
        self.Fy = 0.0
        self.mass = 1724.0
        self.Fz = 200 # Tire normal force

        self.Caf = 120000.0 # Tire cornering stiffness, front
        self.Car = 175000.0 # Stiffness, rear        
        self.alpha = 1.32 # Tire slip angle
        self.mu = 0.55 # Coef Friction tire-ground
        
    def FialaTire(self, alpha, C, eta=0.5):
        z = np.tan(alpha)
        slip = np.arctan2(3*eta*self.mu*self.Fz,C)        
        if(abs(z) < np.tan(slip)):
            Fy = -C*z + (pow(C,2)*np.abs(z)*z)/(3*eta*self.mu*self.Fz)
            Fy -= pow(C,3)*pow(z,3)/(27*pow(eta,2)*pow(self.mu,2)*pow(self.Fz,2))
        else:
            Fy = -eta*self.mu*self.Fz/C

        return Fy

    def Steer(self, angle):
        self.steering = angle

    def Throttle(self, throttle):
        self.Fxr = throttle*2000

    def Step(self, dt):
        self.x[0] += (self.Ux*np.cos(self.yaw)-self.Uy*np.sin(self.yaw))*dt
        self.x[1] += (self.Ux*np.sin(self.yaw)+self.Uy*np.cos(self.yaw))*dt

        self.B  += self.dB*dt
        self.Ux += self.dUx*dt
        self.r  += self.dr*dt
        # self.Uy = self.Ux*np.tan(self.B)
        self.Uy += self.dUy*dt
        self.yaw += self.r*dt

        self.aF = self.steering #- (self.Uy+self.lf*self.r)/self.Ux
        self.aR = self.steering #(self.lf*self.r-self.Uy)/self.Ux
        #self.aR = np.arctan(self.B-(self.lr/self.Ux)*self.r)

        if(np.isnan(self.aF)):
            self.aF = 0.0
        if(np.isnan(self.aR)):
            self.aR = 0.0

        self.Fyf = self.aF*4.0 #self.FialaTire(self.aF,self.Caf)
        self.Fyr = self.aR*4.0 #self.FialaTire(self.aR,self.Car)

        if(np.isnan(self.Fyf)):
            self.Fyf = 0.0
        if(np.isnan(self.Fyr)):
            self.Fyr = 0.0

        self.dB = (1/(self.mass*self.Ux))*(self.Fyf+self.Fyr)-self.r
        if(np.isnan(self.dB)):
            self.dB = 0.0
        self.dr = (1/self.Iz)*(self.lf*self.Fyf-self.lr*self.Fyr)
        self.dUx = (1/self.mass)*(self.Fxr - self.Fyf*np.sin(self.steering))+self.r*self.Ux*self.B
        self.dUy = (1/self.mass)*(self.Fyf + self.Fyr)-self.r*self.Ux        
        

class SimpleModel:
    def __init__(self):
        # state
        self.yaw = 0.0
        self.dyaw = 0.0
        self.x = np.zeros(2)   
        self.v = np.zeros(2)   
        self.a = np.zeros(2)           

        # inputs
        self.steering = 0.0
        self.throttle = 0.0     
        self.C2 =  17.0
        self.Cm1 = 12.0
        self.Cm2 = 2.17
        self.Cr2 = 0.1
        self.Cr0 = 0.6
        self.m = 2.6
        self.Cm0 = 0.1298*500
        self.C0 = 0.0096*10
        self.C1 = 0.0193*10
        self.Fy = 0.0
        self.Ca = 1
        self.Fymax = 10
    
    def Steer(self, angle):
        self.steering = angle

    def Throttle(self, throttle):
        self.throttle = throttle
    
    def Motor(self):
        self.a[0] = (1/self.m)*(self.Cm0*self.throttle-self.C0-self.C1*self.v[0])

    def TireForce(self,slip):
        s = (self.Ca*slip)
        return self.Ca*slip if np.abs(s) < self.Fymax else self.Fymax*np.sign(s)

    def Step(self, dt):         
        self.x[0] += (self.v[0]*np.cos(self.yaw)-self.v[1]*np.sin(self.yaw))*dt
        self.x[1] += (self.v[0]*np.sin(self.yaw)+self.v[1]*np.cos(self.yaw))*dt
        self.yaw += self.dyaw *dt
        self.v[0] += self.a[0]*dt
        self.v[1] += self.a[1]*dt
        
        self.Motor()

class KinBModel:
    def __init__(self):
        # state
        self.yaw = 0.0
        self.dyaw = 0.0
        self.x = np.zeros(2)   
        self.v = np.zeros(2)   
        self.a = np.zeros(2)           
        self.B = 0

        # inputs
        self.steering = 0.0
        self.throttle = 0.0     
        self.C2 =  17.0
        self.Cm1 = 12.0
        self.Cm2 = 2.17
        self.Cr2 = 0.1
        self.Cr0 = 0.6
        self.m = 2.6
        self.Cm0 = 0.1298*500
        self.C0 = 0.0096*10
        self.C1 = 0.0193*10
        self.Fy = 0.0
        self.Ca = 1
        self.Fymax = 10
        self.Fmax = 8.9

        self.v_est = 0.0

        self.V = 0

    def SetAccelIMU( self, x, y,dt):
        self.v_est += x*dt
    
    def Steer(self, angle):
        self.steering = angle*0.5

    def Throttle(self, throttle):
        self.throttle = throttle
    
    def Motor(self):
        self.V = self.Fmax*self.throttle

    def TireForce(self,slip):
        s = (self.Ca*slip)
        return self.Ca*slip if np.abs(s) < self.Fymax else self.Fymax*np.sign(s)

    def Step(self, dt):                         
        self.x[0] += self.v[0]*dt
        self.x[1] += self.v[1]*dt
        self.yaw += self.dyaw *dt

        self.v[0] = self.V*np.cos(self.yaw+self.B)
        self.v[1] = self.V*np.sin(self.yaw+self.B)

        self.B = np.arctan2(0.05*np.tan(self.steering),0.3)
        self.dyaw = (self.V*np.cos(self.B))/(0.3)*(np.tan(self.steering))
        
        self.Motor()


data = np.loadtxt("./modeling/basement1_msg.txt")

# GetIMU
data = data[data[:,0]<1556238584.260252,:]
count = data.shape[0]-1
t0=data[0,0]
print("Start Time: ",t0)
data[:,0] = data[:,0] - data[0,0]
timeDiff = data[1:,0] - data[:-1,0]

imuMask = data[:,6] < 1

imuData = data[imuMask,0:3]
print(imuData.shape)

imuData[:,1] = np.convolve(  imuData[:,1], np.ones(100)/100, mode='same' )
imuData[:,2] = np.convolve(  imuData[:,2], np.ones(100)/100, mode='same' )
print(imuData.shape)

plt.figure()
plt.plot( imuData[:,0], imuData[:,1], label="x")
plt.plot( imuData[:,0], imuData[:,2], label="y")
plt.legend()
plt.title("IMU Readings")
plt.show()

def getData( imu, time):
    select1= imu[:,0] < time+.05
    select2 =imu[:,0] > time-.05
    return np.mean(imu[select1*select2,:],axis=0)

model = KinBModel()

hist = np.zeros((count,15))

for i in range(count):
    if(data[i,6]>0.0):
        model.Steer(data[i,5])
        model.Throttle(data[i,4])   
    hist[i, 0] = data[i,0]
    hist[i, 1:3] = model.x  
    hist[i, 3] = model.V
    hist[i, 4] = model.B
    hist[i, 5:7] = np.array([model.yaw, model.dyaw])
    hist[i, 7] = model.steering
    
    imu = getData( imuData, data[i,0])
    model.SetAccelIMU( imu[1]-0.7, imu[2], timeDiff[i] )

    hist[i, 8] = imu[1]
    hist[i, 9] = imu[2]
    hist[i,10] = model.v_est

    model.Step(timeDiff[i])

    # if i==100: break

plt.figure()
plt.title("Path")
plt.scatter(hist[0:pathEnd:1,1],hist[0:pathEnd:1,2],s=1)
plt.show()

plt.figure()
plt.title("Velocity")
plt.plot(hist[:,0], hist[:,3])
plt.plot(hist[:,0], hist[:,10])
plt.show()

plt.figure()
plt.title("Slip")
plt.plot(hist[:,0], hist[:,4])
plt.show()


plt.figure()
plt.title("Yaw")
plt.plot(hist[:,5:7])
plt.show()

plt.figure()
plt.title("Steering")
plt.plot(hist[:,7])
plt.show()

plt.figure()
plt.title("Accel")
plt.plot(hist[:,8])
plt.plot(hist[:,9])
plt.show()


# # plt.figure()
# # plt.title("Yaw")
# # plt.plot(hist[:,9])
# # plt.plot(hist[:,10])
# # plt.show()

# # plt.figure()
# # plt.plot(hist[:,5], label="Long. Velocity")
# # plt.plot(hist[:,6], label="Long. Accel")
# # plt.legend()
# # plt.show()

# # plt.figure()
# # plt.plot(data[:,4])
# # plt.plot(hist[:,3]/40, label="Engine Rate")
# # plt.plot(hist[:,4], label="Engine Rate Rate")
# # plt.legend()
# # plt.show()
    
# # plt.figure()
# # plt.plot(hist[:,7], label="Long. Load Force")
# # plt.plot(hist[:,8], label="Long. Throttle Force")
# # plt.legend()
# # plt.show()


#     model.Step(timeDiff[i])

# for i in range(count):
#     if(data[i,6]>0.0):
#         model.Steer(data[i,5])
#         model.Throttle(data[i,4])
        
#     model.Step(timeDiff[i])
#     hist[i, 0] = data[i,0]
#     hist[i, 1:3] = model.x    
#     hist[i, 3] = model.B
#     hist[i, 4] = model.dB
#     hist[i, 5] = model.r
#     hist[i, 6] = model.dr
#     hist[i, 7] = model.Ux
#     hist[i, 8] = model.dUx
#     hist[i, 9] = model.Uy
#     hist[i, 10] = model.dUy
#     hist[i, 11] = model.Fyf
#     hist[i, 12] = model.Fyr
#     hist[i, 13] = model.aF
#     hist[i, 14] = model.aR

#     # hist[i,1:3] = model.x
#     # hist[i,3] = model.we
#     # hist[i,4] = model.dwe
#     # hist[i,5] = model.v[0]
#     # hist[i,6] = model.a[0]
#     # hist[i,7] = model.fLoad
#     # hist[i,8] = model.Flon
#     # hist[i,9] = model.yaw
#     # hist[i,10] = model.dyaw
#     # hist[i,11] = model.ddyaw


# plt.figure()
# plt.title("Long Vel and Accel")
# plt.plot(hist[:,7:9])
# plt.show()

# plt.figure()
# plt.title("Side Slip")
# plt.plot(hist[:,3:5])
# plt.show()

# plt.figure()
# plt.title("Yaw Rate, dRate")
# plt.plot(hist[:,5:7])
# plt.show()

# plt.figure()
# plt.title("Tire Forces")
# plt.plot(hist[:,11:13])
# plt.show()

# plt.figure()
# plt.title("Slip Angles")
# plt.plot(hist[:,13:])
# plt.show()

# plt.figure()
# plt.title("Path")
# pathEnd = -1
# plt.scatter(hist[0:pathEnd:100,1],hist[0:pathEnd:100,2],s=1)
# plt.show()

# # plt.figure()
# # plt.title("Yaw")
# # plt.plot(hist[:,9])
# # plt.plot(hist[:,10])
# # plt.show()

# # plt.figure()
# # plt.plot(hist[:,5], label="Long. Velocity")
# # plt.plot(hist[:,6], label="Long. Accel")
# # plt.legend()
# # plt.show()

# # plt.figure()
# # plt.plot(data[:,4])
# # plt.plot(hist[:,3]/40, label="Engine Rate")
# # plt.plot(hist[:,4], label="Engine Rate Rate")
# # plt.legend()
# # plt.show()
    
# # plt.figure()
# # plt.plot(hist[:,7], label="Long. Load Force")
# # plt.plot(hist[:,8], label="Long. Throttle Force")
# # plt.legend()
# # plt.show()





