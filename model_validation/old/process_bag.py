import rosbag
from sensor_msgs.msg import Imu
import os
import numpy as np
import rospy

filename = "/home/chris/basement1_filtered.bag"
outputDir = "./modeling/basement1_msg.txt"
startTime = 0

bag = rosbag.Bag(filename)

topics = bag.get_type_and_topic_info()[1].keys()
types = []

def readImuAccelMsg(rbag):
    topic = "/camera/accel/sample"
    topicCommand = "/racer/car_controller/command"
    numImu = bag.get_message_count(topic)
    numCmd = bag.get_message_count(topicCommand)
    dataArray = np.zeros((numImu+numCmd, 7))
    count = 0
    for topic, msg, t in bag.read_messages(topics=[topic,topicCommand]):           
        if(topic == "/camera/accel/sample"):
            dataArray[count, 0:4] = np.array([t.to_sec(), msg.linear_acceleration.x,
                msg.linear_acceleration.y, msg.linear_acceleration.z])
        else:
            dataArray[count,0] = t.to_sec()
            dataArray[count,4] = msg.linear.x
            dataArray[count,5] = msg.angular.z
            dataArray[count,6] = 1.0
        count += 1
    return dataArray



imuData = readImuAccelMsg(bag)
np.savetxt(outputDir, imuData, fmt="%.3f")

bag.close()
