import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

class SwitchedModel:
    def __init__(self):
        # state
        self.yaw = 0.0
        self.dyaw = 0.0
        self.ddyaw = 0.0
        self.x = np.zeros(2)   
        self.v = np.zeros(2)   
        self.a = np.zeros(2)                   

        # Forces (intermediate variables or inputs)
        self.Fxr = 0.0 # rear tire force
        self.Fmax = 20
        self.Fyf = 0.0
        self.Fyr = 0.0
        self.slipFront = 0.0
        self.slipRear = 0.0
        
        # Configuration
        self.Lr = 0.15 # dist from COG to rear wheel 
        self.Lf = 0.30-self.Lr 
        self.L = 0.30
        self.Iz = 0.24 # Normal inertial
        self.m = 2.0 # Mass      
        self.Fz = self.m*9.8/2  

        # inputs
        self.steering = 0.0 # 0 to 1
        self.throttle = 0.0 # 0 to 1

        # sensors
        self.v_est = 0.0

        # Tire coefficients
        self.B = 7.4        
        self.C = 1.2
        self.D = -2.27
        self.mu = 0.2

    def Reset(self):
        self.yaw = 0.0
        self.dyaw = 0.0
        self.ddyaw = 0.0
        self.x = np.zeros(2)   
        self.v = np.zeros(2)   
        self.a = np.zeros(2)           
        self.Fxr = 0.0 # rear tire force
        self.Fyf = 0.0
        self.Fyr = 0.0
        self.steering = 0.0 # 0 to 1
        self.throttle = 0.0 # 0 to 1

        
    def Steer(self, angle):
        self.steering = angle*0.5

    def Throttle(self, throttle):
        self.throttle = throttle

    def CalcRearSlip(self, Vx, Vy, dyaw):
        return np.arctan2(Vy-self.Lr*dyaw,Vx)-self.steering

    def CalcFrontSlip(self,Vx,Vy,dyaw,steering):
        z = np.arctan2(Vy + self.Lr*dyaw, Vx)
        return z

    def TireSlipPlot(self):
        velocities = np.linspace(0.1,10,100)
        latV = np.linspace(0.1,1,10)
        slips = np.zeros_like(velocities)

        plt.figure()
        for lv in latV.tolist():
            for i in range(100):
                slips[i] = self.CalcRearSlip(velocities[i],lv,0.1)
            plt.plot(velocities, slips,label="{:.2f} lat. vel".format(lv))
        plt.legend()                
        plt.title("Rear Tire Slip vs. Long. Velocity")
        plt.xlabel("Long. Velocity")        
        plt.ylabel("Slip (rad)")
        plt.show()
        plt.figure()
        for lv in latV.tolist():
            for i in range(100):
                slips[i] = self.CalcFrontSlip(velocities[i],lv,0.1,0.2)
            plt.plot(velocities, slips,label="{:.2f} lat. vel".format(lv))
        plt.legend()                
        plt.title("Front Tire Slip vs. Long. Velocity")
        plt.xlabel("Long. Velocity")        
        plt.ylabel("Slip (rad)")
        plt.show()        

    def TirePacejka(self,slip):
        z= -self.mu*self.Fz*np.sin(self.C*np.arctan(self.B*slip))
        z = z if np.abs(z)<2.0 else -2.0*np.sign(slip)
        if(np.isnan(z)):
            return 0.0
        return z

    def TireForcePlot(self):
        slipAngle = np.linspace(-0.8,0.8,50)
        # acr = np.tan( np.arcsin( np.sqrt(Fz**2-Fx**2)/(mu*Fz) )/self.C )/self.B
        result = np.zeros_like(slipAngle) 
        for i in range(len(slipAngle)):
            result[i] = self.TirePacejka( slipAngle[i] )         
        plt.figure()
        plt.plot(slipAngle, result)
        plt.title("Pacejka Tire Force Model")        
        plt.xlabel("Slip Angle")
        plt.show()

    def Step1(self, dt):                         
        self.Fxr = self.throttle*self.Fmax
        self.x[0] += (self.v[0]*np.cos(self.yaw))*dt
        self.x[1] += (self.v[0]*np.sin(self.yaw))*dt
        self.yaw  += (self.dyaw)*dt #+= self.dyaw *dt
        self.v[0] += self.a[0]*dt
        self.a[0] = (1/self.m)*(self.Fxr - 0.2*(self.v[0])**2)           
        self.dyaw =  (self.v[0]/self.L)*np.tan(self.steering)

    def Step2(self, dt):      
        self.x[0] += (self.v[0]*np.cos(self.yaw)-self.v[1]*np.sin(self.yaw))*dt
        self.x[1] += (self.v[0]*np.sin(self.yaw)+self.v[1]*np.cos(self.yaw))*dt
        self.yaw  += self.dyaw *dt
        self.v[0] += self.a[0]*dt
        self.v[1] += self.a[1]*dt                                
        self.dyaw += self.ddyaw*dt

        self.Fxr = self.throttle*self.Fmax
        
        if(self.v[0]>0):
            self.slipFront = self.CalcFrontSlip(self.v[0],self.v[1],self.dyaw,self.steering)
            self.slipRear = self.CalcRearSlip(self.v[0],self.v[1],self.dyaw)
            self.Fyf = self.slipFront*17.0 # self.TirePacejka(self.slipFront)
            self.Fyr = -self.slipRear*17.0 # self.TirePacejka(self.slipRear)

        self.a[0] = (1/self.m)*(self.Fxr-self.Fyf*np.sin(self.steering))+self.v[1]*self.dyaw-0.2*(self.v[0]**2)
        self.a[1] = -(1/self.m)*(self.Fyf*np.cos(self.steering)) -self.v[0]*self.dyaw
        self.ddyaw =  (1/self.Iz)*(self.Lf*self.Fyf*np.cos(self.steering)-self.Lr*self.Fyr)

    def TestLong(self):
        dt = 0.01
        numStep =500
        self.throttle = 1
        results = np.zeros((numStep,6))
        for i in range(numStep):
            self.Step2(dt)
            results[i,0] = self.x[0]
            results[i,1] = self.a[0]
            results[i,2] = self.v[0]    
            results[i,4:6] = np.array([self.slipFront, self.slipRear])

        times=np.linspace(0,numStep*dt,numStep)
        #plt.plot(times,results[:,0], label="Position")
        plt.plot(times,results[:,1],label="Acceleration")
        plt.plot(times,results[:,2], label="Velocity")
        plt.legend()
        plt.xlabel("Seconds")
        plt.title("Longitudinal Model, Max Throttle")
        plt.show()

    def TestLat(self):
        dt = 0.01
        numStep =1650
        times=np.linspace(0,numStep*dt,numStep)
        steering = np.sin(0.5 * np.pi * times)*1
        results = np.zeros((numStep,18))        
        for i in range(numStep):
            self.Step2(dt)
            self.throttle = 1.0       
            self.steering = steering[i]      
            results[i,0:2] = self.x
            results[i,2:4] = self.a
            results[i,10:12] = self.v
            results[i,4:6] = np.array([self.slipFront, self.slipRear])
            results[i,6:8] = np.array([self.Fyf, self.Fyr])
            results[i,8:10] = np.array([self.yaw, self.dyaw])        
            results[i,11] = self.steering
            
        plt.figure()
        plt.scatter(results[:,0],results[:,1], s=1)
        plt.title("Predicted Trajectory, Sinosoidal Steering, Const. Throttle")
        plt.show()

        plt.figure()   
        plt.plot(times,results[:,2],label="Long. Acceleration")
        plt.plot(times,results[:,10], label="Long. Velocity")
        plt.legend()
        plt.show()

        plt.figure()   
        plt.plot(times,results[:,8],label="Yaw Rate")        
        plt.plot(times,results[:,11],label="Steering Input")
        plt.plot(times,results[:,9],label="d/dt Yaw Rate")
        plt.legend()
        plt.show()

        plt.figure()   
        plt.plot(times,results[:,4],label="Slip, Front Tire")
        plt.plot(times,results[:,5],label="Slip, Rear Tire")
        plt.plot(times,results[:,6],label="Lat. Force, Front Tire")
        plt.plot(times,results[:,7],label="Lat. Force, Rear Tire")
        plt.legend()
        plt.title("Slip Angle")
        plt.show()        

if __name__=='__main__':
    model = SwitchedModel()
    model.TireSlipPlot()
    model.TireForcePlot()
    model.TestLong()
    model.Reset()
    model.TestLat()