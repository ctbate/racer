import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from numpy.linalg import lstsq


class KinematicModel(object):
    def __init__(self):
        # state
        self.yaw = 0.0
        self.dyaw = 0.0
        self.ddyaw = 0.0
        self.x = np.zeros(2)
        self.v = np.zeros(2)
        self.a = np.zeros(2)
        self.B = 0.0  # slip
        self.dB = 0.0  # slip rate

        # Configuration
        self.Lr = 0.10  # dist from COG to rear wheel
        self.Lf = 0.30-self.Lr
        self.L = 0.30

        # inputs
        self.steering = 0.0  # 0 to 1
        self.throttle = 0.0  # 0 to 1

        # testing data
        self.testData = None

    def Reset(self):
        self.yaw = 0.0
        self.dyaw = 0.0
        self.ddyaw = 0.0
        self.x = np.zeros(2)
        self.v = np.zeros(2)
        self.a = np.zeros(2)
        self.B = 0.0  # slip
        self.dB = 0.0  # slip rate
        self.steering = 0.0  # 0 to 1
        self.throttle = 0.0  # 0 to 1

    def LongitudinalModel(self, throttle, v):
        # return 1.4*throttle-0.2*v**2-0.2*v*throttle
        return 2.0*(throttle-0.1)-0.2*pow(v, 2)

    def Step(self, dt):
        #self.Fxr = self.throttle*self.Fmax
        self.x[0] += (self.v[0]*np.cos(self.yaw+self.B))*dt
        self.x[1] += (self.v[0]*np.sin(self.yaw+self.B))*dt
        self.yaw += dt*self.dyaw
        self.v[0] = self.v[0]+self.a[0]*dt
        self.a[0] = self.LongitudinalModel(self.throttle, self.v[0])
        self.B = np.arctan(
            (self.Lr/self.L)*np.tan(self.TranslateSteering(self.steering)))
        # np.cos(self.B)*np.tan( self.TranslateSteering(self.steering))
        self.dyaw = (self.v[0]/self.Lr)*np.sin(self.B)

    def TestLong(self):
        dt = 0.01
        numStep = (100)*64
        steering = np.zeros(numStep)*0.1
        throttle = np.zeros(numStep)*0.3
        results = np.zeros((numStep, 6))
        cmdIdx = 0
        for i in range(numStep):
            while(self.testData[cmdIdx, 0] < dt*i):
                self.throttle = self.testData[cmdIdx, 1]
                self.steering = self.testData[cmdIdx, 2]
                if(cmdIdx < len(self.testData)-1):
                    cmdIdx += 1
                else:
                    break
            steering[i] = self.steering
            throttle[i] = self.throttle
            self.Step(dt)
            results[i, 0] = self.x[0]
            results[i, 1] = self.a[0]
            results[i, 2] = self.v[0]

        times = np.linspace(0, numStep*dt, numStep)
        #plt.plot(times,results[:,0], label="Position")
        plt.plot(times, results[:, 1], label="Acceleration")
        plt.plot(times, results[:, 2], label="Velocity")
        plt.plot(times, throttle[:numStep], label="Throttle Input")
        plt.legend()
        plt.xlabel("Seconds")
        plt.title("Longitudinal Model, Max Throttle")
        plt.show()

    def TestLat(self):
        dt = 0.01
        numStep = 100*64
        times = np.linspace(0, numStep*dt, numStep)
        steering = np.zeros(numStep)*0.1
        throttle = np.zeros(numStep)*1.0
        #steering = np.sin(0.5 * np.pi * times)*0.2
        results = np.zeros((numStep, 18))
        cmdIdx = 0
        for i in range(numStep):
            self.Step(dt)
            while(self.testData[cmdIdx, 0] < dt*i):
                self.throttle = self.testData[cmdIdx, 1]
                self.steering = self.testData[cmdIdx, 2]
                if(cmdIdx < len(self.testData)-1):
                    cmdIdx += 1
                else:
                    break
            steering[i] = self.steering
            throttle[i] = self.throttle
            results[i, 0:2] = self.x
            results[i, 2:4] = self.a
            results[i, 10:12] = self.v
            results[i, 4:6] = np.array([self.B, 0.0])
            results[i, 8:10] = np.array([self.yaw, self.dyaw])

        plt.figure()
        plt.scatter(results[:, 0], results[:, 1], s=1)
        plt.title("Predicted Trajectory, Sinosoidal Steering, Const. Throttle")
        plt.show()

        plt.figure()
        plt.plot(times, results[:, 2], label="Long. Acceleration")
        plt.plot(times, results[:, 10], label="Long. Velocity")
        plt.legend()
        plt.show()

        plt.figure()
        plt.plot(times, results[:, 8], label="Yaw")
        plt.plot(times, steering[:numStep], label="Steering Input")
        plt.legend()
        plt.show()

        plt.figure()
        #plt.plot(times, results[:, 4], label="Slip Angle")
        plt.plot(times, steering[:numStep], label="Steering Input")
        plt.plot(times, results[:, 9], label="Yaw Change")
        plt.legend()
        plt.title("Slip Angle over Time")
        plt.show()

    def loadLog(self, filename):
        self.testData = np.loadtxt(filename)
        self.testData = self.testData[:, :]
        t0 = self.testData[0, 0]
        self.testData[:, 0] = self.testData[:, 0] - t0
        print(self.testData[:20, :])

    def TranslateSteering(self, input):
        if(input >= 0.9):
            return -0.437
        if(input >= 0.15):
            return -(0.43*input+0.020)
        elif input >= -0.150:
            return -(0.32*(input+0.150))
        else:
            return -(0.5*input+0.075)

    def SteeringValidation(self):
        wheel_base = 0.3
        straightOffset = -0.150
        steering_input = np.array(
            [1.0, 0.9, 0.75, 0.5, 0.25, -0.15, -0.40, -0.65, -0.90, -0.95, -1.0])
        steering_val = np.array(
            [54.0, 55.0, 63.0, 90, 177, 10000.0, 177, 90, 63, 55, 54])*0.0254*0.5  # radius in meters
        turning_angle = wheel_base / steering_val
        turning_angle[5] = 0.0
        turning_angle[6:] *= -1.0

        print(turning_angle)

        modelCoef = np.zeros(2)
        dataMatrix = np.ones((len(steering_input), 2))
        dataMatrix[:, 0] = steering_input

        soln, _, _, _ = lstsq(dataMatrix, turning_angle[:, None])

        estLine = np.dot(dataMatrix, soln)

        # Chosen model
        area1 = np.ones((5, 2))
        area1[:, 0] = np.linspace(-0.15, 0.25, 5)
        estLine2 = np.dot(area1, np.array([0.32, 0.045]))

        # Right Region
        area2 = np.ones((5, 2))
        area2[:, 0] = np.linspace(0.25, 1.0, 5)
        estLine3 = np.dot(area2, np.array([0.43, 0.020]))

        area3 = np.ones((5, 2))
        area3[:, 0] = np.linspace(-0.15, -1.0, 5)
        estLine4 = np.dot(area3, np.array([0.5, 0.075]))

        print(soln)

        plt.figure()
        plt.plot(steering_input, turning_angle, label="Measured Data")
        # plt.plot(steering_input, estLine, label="Linear Model")
        plt.plot(area1[:, 0], estLine2, label="Selected Model, Middle Region")
        plt.plot(area2[:, 0], estLine3, label="Selected Model, Right Region")
        plt.plot(area3[:, 0], estLine4, label="Selected Model, Left Region")
        plt.title("Steering Angle vs. Input Value")
        plt.xlabel("Input Value")
        plt.legend()
        plt.ylabel("")
        plt.show()


def LongModel():
    throttle = 0.3
    dt = 0.01

    numStep = 637

    a = np.zeros(numStep)
    v = np.zeros(numStep)
    x = np.zeros(numStep)

    ae = 0.0
    ve = 0.0
    xe = 0.0

    for i in range(numStep):
        ae = 12.0*(throttle-0.1)-0.9*(ve**2)
        ve = ve + ae*dt
        xe = xe + ve*dt

        a[i] = ae
        v[i] = ve
        x[i] = xe

    times = np.linspace(0,numStep*dt,numStep)
    plt.figure()
    plt.plot(times, a, label="accel")
    plt.plot(times, v, label="vel")
    plt.plot(times, x, label="pos")
    plt.legend()
    plt.show()

if __name__ == '__main__':
    model = KinematicModel()
    model.loadLog("/home/chris/commands_log.csv")
    model.SteeringValidation()
    # model.TestLong()
    # model.Reset()
    # model.TestLat()

    LongModel()
