import numpy as np
import matplotlib.pyplot as plt 

steering_input = np.array([-0.1, 0.0])
steering_val = np.array([0.0, 0.1]) # radians

throttle_input = np.array([0.0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 1.0])
vel_val = np.array([0.0, 0.75, 1.1, 1.5, 2.25, 3.0, 3.1, 3.3]) # m/sec
vel_val = np.array([0.0, 0.75, 1.1, 1.5, 2.25, 3.0, 3.1, 3.3]) # m/sec

plt.figure()
plt.title("Avg. Velocity vs. Throttle Inpu")
plt.plot(throttle_input, vel_val)
plt.xlabel("Throttle Value")
plt.ylabel("Overall Velocity (m/sec)")
plt.show()

plt.figure()
plt.title("Distance Traveled Over 2 Sec. vs. Throttle Input")
plt.plot(throttle_input, vel_val*2,0)
plt.xlabel("Throttle Value")
plt.ylabel("Distance Traveled (m)")
plt.show()

def velModel(duty,v):
    return 12.0*duty-2.17*(duty*v)-0.1*v**2


plt.figure()
dutyCycles = np.linspace(0,1,5)
velCurves = np.zeros((len(dutyCycles),40))
distCurves = np.zeros((len(dutyCycles),40))
for idxD, dc in enumerate(dutyCycles.tolist()):
    dt = 0.05
    v = 0
    x = 0
    for j in range(40):
        dv = velModel(dc, v)        
        v = v+dv*dt
        x = x+v*dt
        velCurves[idxD,j] = v
        distCurves[idxD,j] = x
    plt.plot( velCurves[idxD,:], label="Velocity, Duty Cycle {}".format(dc) )        
    plt.plot( distCurves[idxD,:], label="Trajectory, Duty Cycle {}".format(dc) )        
plt.legend()

plt.show()
    