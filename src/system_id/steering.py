#!/usr/bin/env python

from rospy import init_node, Subscriber, Publisher, get_param
from rospy import Rate, is_shutdown, ROSInterruptException, spin, on_shutdown
from numpy import pi
from geometry_msgs.msg import Twist
import rospy
import time
import numpy as np
import matplotlib.pyplot as plt
import numpy.random

wheel_radius = 4.125
steering_input = np.array([-1.0, 1.0, 0.9, 0.75, 0.5, 0.25,0.15])
steering_val = np.array([54.0, 54.0, 55.0, 63.0, 90,177]) # radians

throttle_input = np.array([0.0, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 1.0])
vel_val = np.array([0.0, 0.75, 1.1, 1.5, 2.25, 3.0, 3.1, 3.3]) # m/sec                                                                              
 
def steering_id():        
    init_node('steering_id')
    # set node rate
    loop_rate   = 50
    dt          = 1.0 / loop_rate
    rate        = rospy.Rate(loop_rate)
    
    time_prev = time.time()
    cmd_pub = Publisher('/racer/car_controller/command', Twist, queue_size = 10)
    throttle_cmd = 0.0
    steering_cmd = 0.0

    straightLower = -0.15
    srange = 0.000

    startTime = 2

    while not rospy.is_shutdown():        
        if time.time()-time_prev>=startTime+7:
            steering_cmd = 0.0 #reset to straight ahead
            throttle_cmd = 0.0  
            twistMsg = Twist()
            twistMsg.linear.x = throttle_cmd
            twistMsg.angular.z = steering_cmd            
            cmd_pub.publish(twistMsg)                    
        elif time.time()-time_prev>=startTime+5:            
            twistMsg = Twist()
            throttle_cmd = -0.4 # brake
            steering_cmd = 0.0                        
        elif time.time()-time_prev >=startTime:
            randNum = np.random.random()
            steering_cmd = straightLower-randNum*srange
            print(steering_cmd)
            throttle_cmd = 0.3
        else:
            steering_cmd = 0.0
            throttle_cmd = 0.0

        twistMsg = Twist()
        twistMsg.linear.x = throttle_cmd
        twistMsg.angular.z = steering_cmd            
        cmd_pub.publish(twistMsg)
	
        # wait
        rate.sleep()

if __name__ == '__main__':
    try:
        steering_id()        
    except ROSInterruptException as e:
        print(e)        