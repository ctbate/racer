#include <ros/ros.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2/transform_datatypes.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf/tf.h>
#include <sensor_msgs/Imu.h>
#include "VehicleModel.hpp"
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>
#include <geometry_msgs/PoseStamped.h>
#include "RotationEstimator.hpp"
#include "StateEstimation.hpp"

using namespace std;

StateEstimation::StateEstimation(ros::NodeHandle &nh)
{
    m_subCmd = nh.subscribe("car_controller/command", 1, &StateEstimation::commandCallback, this);
    m_subVO = nh.subscribe("/rtabmap/odom", 1, &StateEstimation::voCallback, this);
    m_subAccel = nh.subscribe("/camera/accel/sample", 1, &StateEstimation::imuAccelCallback, this);
    m_subGyro = nh.subscribe("/camera/gyro/sample", 1, &StateEstimation::imuGyroCallback, this);
    m_subDepth = nh.subscribe("/camera/aligned_depth_to_color/image_raw", 1, &StateEstimation::depthCallback, this);
    m_subReset = nh.subscribe("/racer/reset_odometry", 1, &StateEstimation::odomResetCallback, this);
    m_statePublisher = nh.advertise<nav_msgs::Odometry>("/racer/odometry", 1);
    m_modelPublisher = nh.advertise<nav_msgs::Odometry>("/racer/model_odometry", 1);
    m_gyroPublisher = nh.advertise<nav_msgs::Odometry>("/racer/gyro_state", 1);
    m_cmdPublisher = nh.advertise<geometry_msgs::Twist>("/racer/car_controller/command", 1);

    ResetAllOdometry();

    // Log file
    m_logFile.open("/rosbags/commands_log.csv", ofstream::out);
    if (!m_logFile.is_open())
    {
        throw std::runtime_error("Failed to open log file.");
    }
}

StateEstimation::~StateEstimation()
{
    m_logFile << std::flush;
    m_logFile.close();
}

void StateEstimation::ResetAllOdometry()
{
    // reset
    m_prior = m_prior.setIdentity();
    m_processNoiseCov = m_processNoiseCov.setIdentity() * 0.2;
    m_measurementNoiseCov = m_measurementNoiseCov.setIdentity() * 0.2;
    m_measurementJacobian.setIdentity();
    m_estState.pos[0] = m_estState.pos[1] = m_estState.yaw = m_estState.v = 0.0;
    m_measurements.posLocal[0] = m_measurements.posLocal[1] = 0.0;
    m_measurements.vel = 0.0;
    m_measurements.measuredControls.steering = 0.0;
    m_measurements.measuredControls.throttle = 0.0;
    m_measurements.yawLocal = 0.0;
    m_measurements.yawLast = 0.0;
    m_measurements.yawFirst = 0.0;
    m_measurements.distRightWall = 0.0;

    // Trajectory
    m_segmentIdx = 0;

    m_distLeft = 0.0;
    m_distRight = 0.0;
    m_distFront = 0.0;
    // m_error.deviation = 0.0;

    m_useOdomPosition = false;
    m_useOdomVelocity = true;
    m_printKalmanInfo = false;
    m_useIMU = true;

    m_rotationEstimator.reset();

    model.m_kinBicInternalState.pos[0] = model.m_kinBicInternalState.pos[1] = 0.0;
    model.m_kinBicInternalState.v = 0.0;
    model.m_kinBicInternalState.yaw = 0.0;

    m_segments.clear();

    TrajectorySegment seg1, seg2, seg3;
    seg1.upperBoundX = 19.0;
    seg2.lowerBoundY = -30.0;
    seg3.lowerBoundX = 0.0;

    m_segments.push_back(seg1);
    m_segments.push_back(seg2);
    m_segments.push_back(seg3);
}

void StateEstimation::commandCallback(const geometry_msgs::Twist::ConstPtr &msg)
{
    static ros::Time t0 = ros::Time::now();

    // TODO : SYSTEM ID STEERING TO ANGULAR
    m_measurements.measuredControls.steering = msg->angular.z;

    // TODO : SYSTEM ID THROTTLE TO ACCEL or VEL
    m_measurements.measuredControls.throttle = msg->linear.x;

    m_logFile << (ros::Time::now() - t0) << " " << msg->linear.x << " " << msg->angular.z << endl;
}

void StateEstimation::imuAccelCallback(const sensor_msgs::ImuConstPtr &msg)
{
    // m_rotationEstimator.process_accel(msg);
    //m_measurements.yawLocal = m_rotationEstimator.get_theta().y;
}

void StateEstimation::odomResetCallback(const std_msgs::Bool::ConstPtr &msg)
{
    if (msg->data == true)
    {
        ROS_INFO("STATE ESTIMATION NODE RESETTING ODOMETRY");
        ResetAllOdometry();
    }
}

void StateEstimation::depthCallback(const sensor_msgs::Image::ConstPtr &msg)
{
    // numImagesRecv++;
    // // Simulation gives FC32 encoding,
    // // Realsense gives 16UC1 encoding
    const unsigned int senseRowStart = 30;
    const unsigned int senseRowEnd = 60;
    const unsigned int numRow = 30;

    const unsigned int leftLimit = 50;
    const unsigned int rightLimit = 50;

    const unsigned int imgWidth = msg->width;
    uint16_t(*msgMatrix)[imgWidth] = (uint16_t(*)[imgWidth])msg->data.data();

    m_distLeft = 0.0;
    m_distRight = 0.0;
    m_distFront = 0.0;

    for (unsigned senseRow = senseRowStart; senseRow < senseRowEnd; senseRow++)
    {
        for (unsigned int i = 0; i < leftLimit; i++)
        {
            m_distLeft += (float)msgMatrix[senseRow][i];
        }

        for (unsigned int i = leftLimit; i < imgWidth - rightLimit; i++)
        {
            m_distFront += (float)msgMatrix[senseRow][i];
        }

        for (unsigned int i = imgWidth - rightLimit; i < imgWidth; i++)
        {
            m_distRight += (float)msgMatrix[senseRow][i];
        }
    }
    m_distLeft = (m_distLeft) / (leftLimit * 1000 * numRow);
    m_distFront = (m_distFront) / ((imgWidth - leftLimit - rightLimit) * numRow * 1000);
    m_distRight = (m_distRight) / (rightLimit * numRow * 1000);

    if (isnan(m_distLeft))
        m_distLeft = 0.0;

    if (isnan(m_distRight))
        m_distRight = 0.0;

    // The constant is cos(1.11)
    m_measurements.distRightWall = 0.44466151674 * m_distRight;

    cout.precision(2);
    cout << "RIGHT WALL: " << m_measurements.distRightWall << endl;
}

void StateEstimation::imuGyroCallback(const sensor_msgs::ImuConstPtr &msg)
{
    static ros::Time lastTime;
    static bool first = true;
    double dt = 0.0;

    if (first)
    {
        lastTime = msg->header.stamp;
        first = false;
        return;
    }

    dt = (msg->header.stamp - lastTime).toSec();

    lastTime = msg->header.stamp;

    m_rotationEstimator.process_gyro(msg, dt);
    m_measurements.yawLocal = m_rotationEstimator.get_theta().y;
}

void StateEstimation::voCallback(const nav_msgs::Odometry::ConstPtr &msg)
{
    m_measurements.posLocal[0] = msg->pose.pose.position.x;
    m_measurements.posLocal[1] = msg->pose.pose.position.y;
    m_measurements.vel = msg->twist.twist.linear.x; // *0.5 + 0.5*msg->twist.twist.linear.x;
}

void StateEstimation::voCallbackPose(const geometry_msgs::PoseStamped::ConstPtr &msg)
{
    m_measurements.posLocal[0] = msg->pose.position.x;
    m_measurements.posLocal[1] = msg->pose.position.x;
}

void StateEstimation::PublishTransform()
{
    // Create our odometry frame -> baselink  transformation
    tf2::Quaternion bodomQuat;
    if (isnan(m_estState.yaw))
    {
        m_estState.yaw = 0.0;
    }
    bodomQuat.setRPY(0.001, 0.001, m_measurements.yawLocal);

    // create our transform for tf
    geometry_msgs::TransformStamped transform;
    transform.header.stamp = ros::Time::now();
    transform.header.frame_id = "odom";
    transform.child_frame_id = "base_link";
    transform.transform.translation.x = m_estState.pos[0];
    transform.transform.translation.y = m_estState.pos[1];
    transform.transform.translation.z = 0.0;
    tf2::convert(bodomQuat, transform.transform.rotation);

    // Publish to tf
    m_tfBr.sendTransform(transform);

    // Publish the odometry - kalman filtered
    nav_msgs::Odometry msg;
    msg.header.stamp = ros::Time::now();
    msg.header.frame_id = "odom";
    msg.child_frame_id = "base_link";
    msg.pose.pose.position.x = m_estState.pos[0];
    msg.pose.pose.position.y = m_estState.pos[1];
    msg.pose.pose.position.z = 0.0;
    msg.pose.pose.orientation = transform.transform.rotation;
    msg.twist.twist.linear.x = m_estState.v;
    m_statePublisher.publish(msg);

    // Publish the odometry - model only, non-filtered
    tf2::Quaternion modelQuat;
    nav_msgs::Odometry msgModel;
    modelQuat.setRPY(0.001, 0.001, model.m_kinBicInternalState.yaw);
    msgModel.header.stamp = ros::Time::now();
    msgModel.header.frame_id = "odom";
    msgModel.child_frame_id = "base_link";
    msgModel.pose.pose.position.x = model.m_kinBicInternalState.pos[0];
    msgModel.pose.pose.position.y = model.m_kinBicInternalState.pos[1];
    msgModel.pose.pose.position.z = 0.0;
    msgModel.twist.twist.linear.x = model.m_kinBicInternalState.v;
    tf2::convert(modelQuat, msgModel.pose.pose.orientation);
    m_modelPublisher.publish(msgModel);

    // Publish odometry msg - model position with orientation given
    // by rotation estimator
    // tf2::Quaternion modelQuat;
    // nav_msgs::Odometry msgModel;
    modelQuat.setRPY(0.001, 0.001, m_rotationEstimator.get_theta().y);
    msgModel.header.stamp = ros::Time::now();
    msgModel.header.frame_id = "odom";
    msgModel.child_frame_id = "base_link";
    msgModel.pose.pose.position.x = model.m_kinBicInternalState.pos[0];
    msgModel.pose.pose.position.y = model.m_kinBicInternalState.pos[1];
    msgModel.pose.pose.position.z = 0.0;
    msgModel.twist.twist.linear.x = model.m_kinBicInternalState.v;
    tf2::convert(modelQuat, msgModel.pose.pose.orientation);
    m_gyroPublisher.publish(msgModel);
}

void StateEstimation::Step(double dt)
{

    // Step the trajectory estimation
    if (m_estState.pos[0] >= m_segments[m_segmentIdx].upperBoundX)
    {
        m_segmentIdx++;
    }
    else if (m_estState.pos[1] <= m_segments[m_segmentIdx].lowerBoundY)
    {
        m_segmentIdx++;
    }
    else if (m_estState.pos[1] <= m_segments[m_segmentIdx].lowerBoundX)
    {
        m_segmentIdx++;
    }

    cout << "SEGMENT: " << m_segmentIdx << endl;

    cout << m_estState.pos[0] << " " << m_estState.pos[1] << endl;

    double correction = 0.0;

    cout << "FRONT: " << m_distFront << endl;

    if (m_segmentIdx == 0)
    {
        // Tell the expected error - which is just the y value
        correction = (m_measurements.distRightWall - 1.5);
        cout << "WALL POS: " << m_measurements.distRightWall << endl;
        cout << "MEASURED ERROR: " << correction << endl;
        cout << "Expected Error: " << (m_estState.pos[1]) << endl;

        if (abs(correction) < 2.0)
        {
            m_estState.pos[1] = (m_measurements.distRightWall - 1.5);
        }
    }
    else if (m_segmentIdx == 1)
    {
        correction = (19 + (m_measurements.distRightWall - 1.5));
        // Tell the expected error - which is just the y value
        cout << "MEASURED ERROR: " << correction << endl;
        cout << "Expected Error: " << (m_estState.pos[0]) << endl;

        if (abs(correction - 19) < 2.0)
        {
            m_estState.pos[0] = (19 + (m_measurements.distRightWall - 1.5));
        }
    }
    else
    {
        // Tell the expected error - which is just the y value
        correction = (-30 + (1.5 - m_measurements.distRightWall));
        cout << "MEASURED ERROR: " << correction << endl;
        cout << "Expected Error: " << (m_estState.pos[1]) << endl;
        if (abs(correction + 30) < 2.0)
        {
            m_estState.pos[1] = correction;
        }
    }

    // = false;
    // drive forward
    geometry_msgs::Twist msg;
    double speed;
    speed = 0.3;
    double angle = 0.0;

    float p_value;

    // Speed control
    if (m_distFront < 5)
    {
        speed = 0.3;
        if (m_distFront < 1)
        {
            speed = 0.0;
        }
    }

    // steering
    float error = m_distRight - m_distLeft;

    p_value = error;

    if (p_value > 1.0)
    {
        p_value = 1.0;
    }
    else if (p_value < -1.0)
    {
        p_value = -1.0;
    }
    angle = -p_value; //* m_params.turn_coefP;
    msg.linear.x = speed;
    msg.angular.z = -angle;
    // m_cmdPublisher.publish(msg);

    // Perform Kalman Filtering
    StepEKF(dt);

    // Step the bicycle model's internal state (seperate from the kalman filtered state),
    // it is used as continuous odometry
    model.Step(dt, m_measurements.measuredControls);
}

void StateEstimation::StepEKF(double dt)
{
    Eigen::Vector4d currMeas = {m_measurements.posLocal[0], m_measurements.posLocal[1],
                                m_measurements.yawLocal, m_measurements.vel};

    // Predict next state
    VehicleModel::KinematicBicycleState predState;
    predState = model.ProcessModel_KinematicBicycle(m_estState, m_measurements.measuredControls, dt);
    predState.v = m_measurements.vel;

    // Numerical Jacobian
    auto jac = NumericalJacobian(m_estState, dt);

    // Propogate covariance estimate
    Eigen::Matrix<double, 4, 4> Pkp1 = (jac * m_prior) * jac.transpose() + m_processNoiseCov;

    // Predict measurements.
    // Here we are assuming that our predicted measurements correspond directly to the
    // state vector since we are observing state position (x/y/v) from VO
    // and (v,yaw) from IMU
    Eigen::Vector4d predMeas;
    VehicleModel::StateToVector(predState, predMeas);

    // Jacobian for measurements.
    if (!m_useOdomPosition)
    {
        m_measurementJacobian(0, 0) = 0.000;
        m_measurementJacobian(1, 1) = 0.000;
    }
    if (!m_useOdomVelocity)
    {
        m_measurementJacobian(3, 3) = 0.0;
    }
    if (!m_useIMU)
    {
        m_measurementJacobian(2, 2) = 0.0;
    }

    // Cross covariance.
    Eigen::Matrix<double, 4, 4> P12 = Pkp1 * m_measurementJacobian;

    // calculate gain
    Eigen::Matrix4d gain = (m_measurementJacobian * P12 + m_measurementNoiseCov);
    gain = P12 * gain.inverse();

    // hack
    if (m_useIMU)
    {
        gain(0, 2) = 0.0;
        gain(1, 2) = 0.0;
        gain(3, 2) = 0.0;
    }

    // state estimate
    Eigen::Vector4d predStateVector;
    VehicleModel::StateToVector(predState, predStateVector);
    Eigen::Matrix4d iden;
    iden.setIdentity();
    auto residual = currMeas - predMeas;
    auto x_est = predStateVector + gain * (residual);

    if (m_printKalmanInfo)
    {
        cout << "Process Jacobian" << endl
             << jac << endl;
        cout << "P_kp1" << endl
             << Pkp1 << std::endl;
        cout << "P12" << endl
             << P12 << std::endl;
        cout << "Gain" << endl
             << gain << std::endl;
        cout << "Measured" << endl
             << currMeas << std::endl;
        cout << "Predicted" << endl
             << predMeas << std::endl;
        cout << "Residual" << endl
             << predMeas - currMeas << endl;
        cout << "Measured Controls: " << m_measurements.measuredControls.steering << ", " << m_measurements.measuredControls.throttle << endl;
        cout << "NewState" << endl
             << x_est << std::endl;
    }
    Pkp1 = ((gain * m_measurementNoiseCov) * gain.transpose()) + (((iden - (gain * m_measurementJacobian)) * Pkp1) * (iden - (gain * m_measurementJacobian)).transpose());
    VehicleModel::VectorToState(m_estState, x_est);
    m_prior = Pkp1;
}

Eigen::Matrix<double, 4, 4> StateEstimation::NumericalJacobian(VehicleModel::KinematicBicycleState &state, double dt)
{
    Eigen::Vector4d perturbStateHi, perturbStateLow;
    Eigen::Matrix<double, 4, 4> jac;
    Eigen::Vector4d vector;

    VehicleModel::StateToVector(m_estState, vector);

    for (unsigned int i = 0; i < 4; i++)
    {
        Eigen::Vector4d pert = vector;
        pert[i] += 1e-5 / 2;
        perturbStateHi = model.ProcessModel_KinematicBicycle(pert, m_measurements.measuredControls, dt);

        pert = vector;
        pert[i] -= (1e-5 / 2);
        perturbStateLow = model.ProcessModel_KinematicBicycle(pert, m_measurements.measuredControls, dt);

        perturbStateLow = model.ProcessModel_KinematicBicycle(pert, m_measurements.measuredControls, dt);
        jac.block<1, 4>(i, 0) = (perturbStateHi - perturbStateLow) / 1e-5;
    }

    return jac;
}
