
#include <ros/ros.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2/transform_datatypes.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf/tf.h>
#include <sensor_msgs/Imu.h>
#include "VehicleModel.hpp"
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>
#include "RotationEstimator.hpp"
#include "StateEstimation.hpp"

using namespace std;

int main(int argc, char **argv)
{
    ros::init(argc, argv, "state_estimation");

    ros::NodeHandle nodeHandle("racer");

    StateEstimation stateEstimation(nodeHandle);

    ros::Time currentTime, lastTime;
    currentTime = ros::Time::now();
    lastTime = ros::Time::now();

    ros::Rate rate(50);
    ros::AsyncSpinner spinner(1);

    spinner.start();    

    while (ros::ok())
    {
        currentTime = ros::Time::now();
        try
        {
            stateEstimation.Step((currentTime - lastTime).toSec());
            stateEstimation.PublishTransform();
        }
        catch (std::exception &e)
        {
            cerr << e.what() << endl;
        }

        lastTime = currentTime;
        rate.sleep();
    }

    return 0;
}