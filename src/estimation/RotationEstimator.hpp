#ifndef ROTATION_EST_H
#define ROTATION_EST_H
#include <iostream>
#include <librealsense2/rs.hpp>
#include <mutex>
#include <cmath>
#include <exception>
#include <sensor_msgs/Imu.h>


class RotationEstimator
{
  public:
    struct double3
    {
        double x, y, z;
        double3 operator*(double t)
        {
            return {x * t, y * t, z * t};
        }

        double3 operator-(double t)
        {
            return {x - t, y - t, z - t};
        }

        void operator*=(double t)
        {
            x = x * t;
            y = y * t;
            z = z * t;
        }

        void operator=(double3 other)
        {
            x = other.x;
            y = other.y;
            z = other.z;
        }

        void add(double t1, double t2, double t3)
        {
            x += t1;
            y += t2;
            z += t3;
        }
    };
    struct double2
    {
        double x, y;
    };

  private:
    const double PI = 3.14159265358979323846;

    const size_t IMU_FRAME_WIDTH = 1280;
    const size_t IMU_FRAME_HEIGHT = 720;

    // theta is the angle of camera rotation in x, y and z components
    double3 theta;
    std::mutex theta_mtx;

    /* alpha indicates the part that gyro and accelerometer take in computation of theta; higher alpha gives more weight to gyro, but too high
    values cause drift; lower alpha gives more weight to accelerometer, which is more sensitive to disturbances */
    double alpha = 0.98;
    bool first = true;

    // Keeps the arrival time of previous gyro frame
    double last_ts_gyro = 0;

  public:
    void reset()
    {
        theta.x = 0.0;
        theta.y = 0.0;
        theta.z = 0.0;
    }

    // Function to calculate the change in angle of motion based on data from gyro
    void process_gyro(const sensor_msgs::Imu::ConstPtr &msg, double dt)
    {
        if (first) 
        {
            first = false;
            theta.y = 0.0;
        }
        // Holds the change in angle, as calculated from gyro
        double3 gyro_angle;

        // Initialize gyro_angle with data from gyro
        gyro_angle.x = msg->angular_velocity.y; // Pitch
        gyro_angle.y = msg->angular_velocity.z; // Yaw
        gyro_angle.z = msg->angular_velocity.x; // Roll
        
        // Compute the difference between arrival times of previous and current gyro frames
        if( dt < 0.0){
             return;
        }

        // Change in angle equals gyro measures * time passed since last measurement
        gyro_angle = gyro_angle * dt;
        if(gyro_angle.y >= 0.5){
            std::cout <<"DT: "<< dt << std::endl;
            throw std::runtime_error("Large y angle: "+std::to_string(gyro_angle.y));
        }

        // Apply the calculated change of angle to the current angle (theta)
        std::lock_guard<std::mutex> lock(theta_mtx);
        theta.add(gyro_angle.z, gyro_angle.y, gyro_angle.x);
        //theta.y = prevTheta * alpha + theta.y * (1 - alpha);
        //gyro_angle.y = remainder(gyro_angle.y, PI);
    }

    void process_accel(const sensor_msgs::Imu::ConstPtr &msg)
    {
        // Holds the angle as calculated from accelerometer data
        double3 accel_angle;

        // Calculate rotation angle from accelerometer data
        accel_angle.z = atan2( msg->linear_acceleration.y, msg->linear_acceleration.x);
        accel_angle.x = atan2( msg->linear_acceleration.y, sqrt(msg->linear_acceleration.z * msg->linear_acceleration.z + msg->linear_acceleration.x * msg->linear_acceleration.x));

        // If it is the first iteration, set initial pose of camera according to accelerometer data (note the different handling for Y axis)
        std::lock_guard<std::mutex> lock(theta_mtx);
        if (first)
        {
            first = false;
            theta = accel_angle;
            // Since we can't infer the angle around Y axis using accelerometer data, we'll use PI as a convetion for the initial pose
            theta.y = 0.0;
        }
        else
        {
            /* 
            Apply Complementary Filter:
                - high-pass filter = theta * alpha:  allows short-duration signals to pass through while filtering out signals
                  that are steady over time, is used to cancel out drift.
                - low-pass filter = accel * (1- alpha): lets through long term changes, filtering out short term fluctuations 
            */
            theta.x = theta.x * alpha + accel_angle.x * (1 - alpha);
            theta.z = theta.z * alpha + accel_angle.z * (1 - alpha);
        }
    }

    // Returns the current rotation angle
    double3 get_theta()
    {
        std::lock_guard<std::mutex> lock(theta_mtx);
        return theta;
    }
};

#endif