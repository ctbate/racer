#ifndef STATE_ESTIMATION_NODE_H
#define STATE_ESTIMATION_NODE_H

#include <ros/ros.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2/transform_datatypes.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf/tf.h>
#include <sensor_msgs/Imu.h>
#include <std_msgs/Bool.h>
#include "VehicleModel.hpp"
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>
#include "RotationEstimator.hpp"
#include <geometry_msgs/PoseStamped.h>
#include <fstream>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Image.h>

class StateEstimation
{
  public:
    StateEstimation(ros::NodeHandle &nh);

    ~StateEstimation();  

    void ResetAllOdometry();  

    void commandCallback(const geometry_msgs::Twist::ConstPtr &msg);

    void imuAccelCallback(const sensor_msgs::ImuConstPtr &msg);    
    void imuGyroCallback(const sensor_msgs::ImuConstPtr &msg);    
    void voCallback(const nav_msgs::Odometry::ConstPtr &msg);
    void voCallbackPose(const geometry_msgs::PoseStamped::ConstPtr &msg);
    void depthCallback(const sensor_msgs::Image::ConstPtr &msg);
    void odomResetCallback( const std_msgs::Bool::ConstPtr &msg);

    void PublishTransform();
    void Step(double dt);
    void StepEKF(double dt);

    Eigen::Matrix<double, 4, 4> NumericalJacobian(VehicleModel::KinematicBicycleState &state, double dt);

  private:
    VehicleModel model;
    RotationEstimator m_rotationEstimator;

    ros::Subscriber m_subDepth;
    ros::Subscriber m_subCmd;
    ros::Subscriber m_subAccel;
    ros::Subscriber m_subGyro;
    ros::Subscriber m_subVO;
    ros::Subscriber m_subReset;    

    ros::Publisher m_statePublisher;
    ros::Publisher m_modelPublisher;
    ros::Publisher m_gyroPublisher;
    ros::Publisher m_cmdPublisher;
    tf2_ros::TransformBroadcaster m_tfBr;

    struct Measurements
    {
        double posLocal[2];
        double yawLocal = 0.0;
        double yawLast = 0.0;
        double yawFirst = 0.0;
        double vel = 0.0;        
        double distRightWall = 0.0;
        double error = 0.0; 
        VehicleModel::KinematicBicycleControl measuredControls;
    };

    

    struct TrajectoryState
    {
      double m_dist = 0.0;
      double m_error = 0.0;      
    };

    struct TrajectorySegment
    {
      double upperBoundX = 900.0;
      double lowerBoundY = -900.0;
      double lowerBoundX = -900.0;
    };

    


    VehicleModel::KinematicBicycleState m_estState;
    Measurements m_measurements;    

    // Path Tracking
    std::vector<TrajectorySegment> m_segments;
    unsigned int m_segmentIdx;

    // EKF    
    Eigen::Matrix<double, 4, 4> m_prior;
    Eigen::Matrix<double, 4, 4> m_processNoiseCov;
    Eigen::Matrix<double, 4, 4> m_measurementNoiseCov;
    Eigen::Matrix4d m_measurementJacobian;
    bool m_printKalmanInfo;
    double m_distRight;
    double m_distLeft;
    double m_distFront;
    bool m_useOdomPosition;
    bool m_useIMU;
    bool m_useOdomVelocity;
    std::ofstream m_logFile;
};

#endif // !STATE_ESTIMATION_NODE_H
