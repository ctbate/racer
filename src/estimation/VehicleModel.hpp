#ifndef CAR_MODEL_H
#define CAR_MODEL_H
#include <eigen3/Eigen/Core>

class VehicleModel
{
  public:
    VehicleModel();

    struct KinematicBicycleState
    {
        double pos[2] = {0.0, 0.0};
        double yaw = 0.0;
        double v = 0.0;
    };

    struct KinematicBicycleControl
    {
        double throttle;
        double steering;
    };

    void Step(double dt,  KinematicBicycleControl &control);

    void Reset();
    
    KinematicBicycleState ProcessModel_KinematicBicycle(
        KinematicBicycleState &state, KinematicBicycleControl &control, double dt);

    Eigen::Vector4d ProcessModel_KinematicBicycle(
        Eigen::Vector4d &state, KinematicBicycleControl &control, double dt)
    {
        KinematicBicycleState stateConv;
        VectorToState(stateConv, state);
        Eigen::Vector4d newState;        
        StateToVector(ProcessModel_KinematicBicycle(stateConv, control, dt),newState);
        return newState;
    }

    void MeasurementModel_KinematicBicycle();

    static void StateToVector(const KinematicBicycleState &state, Eigen::Vector4d &vector)
    {
        vector = {state.pos[0], state.pos[1], state.yaw, state.v};
    }

    static void VectorToState(KinematicBicycleState &state, const Eigen::Vector4d &vector)
    {
        state.pos[0] = vector[0];
        state.pos[1] = vector[1];
        state.yaw = vector[2];
        state.v = vector[3];
    }

    KinematicBicycleState m_kinBicInternalState;

    // Constants
    double m_mass;
    double m_Lf;
    double m_Lr;
    double m_L;
};

#endif // !CAR_MODEL_H