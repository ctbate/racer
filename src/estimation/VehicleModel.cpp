#include "VehicleModel.hpp"
#include <cmath>
#include <iostream>
#include <algorithm>

using namespace std;

VehicleModel::VehicleModel()
{
    m_mass = 2;
    m_Lf = 0.20;
    m_Lr = 0.10;
    m_L = m_Lf + m_Lr;

    // Reset internal state.
    Reset();
}

void VehicleModel::Reset()
{
    // Reset state
    m_kinBicInternalState.pos[0] = m_kinBicInternalState.pos[1] = 0.0;
    m_kinBicInternalState.yaw = 0.0;
    m_kinBicInternalState.v = 0.0;
}

VehicleModel::KinematicBicycleState VehicleModel::ProcessModel_KinematicBicycle(KinematicBicycleState &state, KinematicBicycleControl &control, double dt)
{
    if (dt > 0.025 || dt < 0.0)
    {
        throw std::runtime_error("Incorrect dt in model: " + to_string(dt));
    }
    // Steering model
    //double df = -(0.47*control.steering);
    double df = 0.0;

    if (abs(control.steering) > 0.20)
    {
        df = -0.35 * control.steering;
    }
    else if (abs(control.steering) > 0.15)
    {
        df = -0.30 * (control.steering);
    }
    else if (abs(control.steering) > 0.05)
    {
        df = -0.20 * (control.steering);
    }

    // Clam the df
    if (df < -0.43)
    {
        df = -0.43;
    }
    else if (df > 0.43)
    {
        df = 0.43;
    }

    // Longitudinal model
    double accel = 0.0;
    if (control.throttle >= 0)
    {
        accel = max(12.0 * (control.throttle - 0.19), 0.0) -0.2*state.v - 0.2 * pow(state.v, 2);
    }
    else
    {
        accel = min(0.0, -2.0 * (control.throttle - 0.1)) - 0.2 * pow(state.v, 2);
    }

    // compute next state
    double slip = atan((m_Lr / m_L) * tan(df));
    KinematicBicycleState next;
    next.pos[0] = state.pos[0] + dt * (state.v * cos(state.yaw + slip));
    next.pos[1] = state.pos[1] + dt * (state.v * sin(state.yaw + slip));
    next.yaw = state.yaw + dt * (state.v / m_Lr) * sin(slip);
    next.v = state.v + dt * accel;

    return next;
}

void VehicleModel::MeasurementModel_KinematicBicycle()
{
}

void VehicleModel::Step(double dt, KinematicBicycleControl &control)
{
    m_kinBicInternalState = ProcessModel_KinematicBicycle(m_kinBicInternalState, control, dt);
}