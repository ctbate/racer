/**
 * Actuators.hpp
 * Implements the subscriber that accepts CarControl and CarAcceleration messages,
 * and makes the required calls to the ServoController
 *
 * February 2019
 *
 * Author: Chris
 */
#ifndef RHW1_SUB_
#define RHW1_SUB_

#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/robot_hw.h>
#include "ServoController.hpp"
#include <ros/ros.h>
#include <cstdint>

#include <vector>
#include <string>

namespace racer
{
class CarHardwareInterface : public hardware_interface::RobotHW
{
  public:
	CarHardwareInterface();

	void reset();

	void read();
	void write();

  private:
	ServoController m_servoController;
	hardware_interface::EffortJointInterface m_effortInterface;
	hardware_interface::VelocityJointInterface m_velocityInterface;
	hardware_interface::PositionJointInterface m_positionInterface;
	hardware_interface::JointStateInterface m_jntStateInterface;	
	std::vector<double> m_cmd;
	std::vector<double> m_pos;
	std::vector<double> m_vel;
	std::vector<double> m_eff;
	std::vector<std::string> m_jointNames;
};

} // namespace racer

#endif