/**
 * ServoController main class
 * Interfaces with Maestro 18 servo controller
 * February, 2019
 *  
 * Author: Chris
 */
#include "ServoController.hpp"

#include <cerrno>
#include <cstring>
#include <ros/ros.h>
#include <thread>
#include <chrono>

using namespace racer;
using namespace std;

ServoController::ServoController() : m_state(NOT_READY)
{
    // Default constructor
}

ServoController::~ServoController()
{
    if (m_tty.is_open())
    {
        // Reset the state
        WriteRaw(0, 6000);
        WriteRaw(1, 6000);
        m_tty.close();
    }
}

ServoController::ServoController(Settings &settings)
{
    Init(settings);
}

void ServoController::Init(Settings &settings)
{
    m_settings = settings;

    //  we will close and reopen the tty if necessary
    if (m_tty.is_open())
    {
        m_tty.close();
    }

    m_tty = ofstream(m_settings.filename, ofstream::out | ofstream::binary);
    m_state = READY;

    if (!m_tty.is_open())
    {
        m_state = NOT_READY;
        return;
    }

    // Reset the state
    WriteRaw(0, 6000);
    WriteRaw(1, 6000);
}

void ServoController::WriteRaw(uint8_t servoNo, uint16_t target)
{
    char msg[4];
    msg[0] = 0x84; // Command code
    msg[1] = servoNo;
    msg[2] = target & 0x7F; // Data bytes can only have the first 7 bit sent
    msg[3] = (target >> 7) & 0x7F;

    m_tty.write(msg, 4);
    m_tty.flush();

    if (!m_tty)
    {
        // There is an issue, so reset the state to not ready
        m_state = NOT_READY;
    }
}

void ServoController::SendCommand(uint8_t servoNo, uint16_t target)
{
    std::lock_guard<std::mutex> guard(m_mutex);

    if (m_state != READY)
    {
        Init(m_settings);
    }
    else
    {
        WriteRaw(servoNo, target);
    }
}