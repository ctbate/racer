/**
 * main_sub.cpp
 * Implments subscriber for the actuators executable node
 * "rosrun racer actuators"
 * Author: Chris
 */
#include <controller_manager/controller_manager.h>
#include <ros/ros.h>
#include "CarHardwareInterface.hpp"

#include <chrono>
#include <thread>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "CarHardwareInterface");

    ros::NodeHandle nodeHandle("racer");    
    racer::CarHardwareInterface hardwareInterface;
    controller_manager::ControllerManager controllerManager(&hardwareInterface, nodeHandle);

    ros::AsyncSpinner spinner(2);
    spinner.start();
    ros::Rate rate(50);
    auto ts = ros::Time::now();
    while (ros::ok())
    {
        auto elapsed = ros::Time::now() - ts;
        ts = ros::Time::now();
        hardwareInterface.read();
        controllerManager.update(ts, elapsed);
        hardwareInterface.write();
        rate.sleep();                
    }

    return 0;
}