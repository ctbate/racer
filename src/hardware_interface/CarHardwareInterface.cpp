/**
 * Actuators.hpp
 * Implements the subscriber that accepts CarControl and CarAcceleration messages,
 * and makes the required calls to the ServoController
 *
 * February 2019
 *
 * Author: Chris
 */
#include "CarHardwareInterface.hpp"

using namespace racer;

CarHardwareInterface::CarHardwareInterface()
{
    m_jointNames.push_back("steering_hinge_joint");
    m_jointNames.push_back("rear_axel_joint");    

    m_pos.resize(m_jointNames.size());
    m_vel.resize(m_jointNames.size());
    m_eff.resize(m_jointNames.size());
    m_cmd.resize(m_jointNames.size());

    for (unsigned i = 0; i < m_jointNames.size(); i++)
    {
        hardware_interface::JointStateHandle state_handle(m_jointNames[i], &m_pos[i],
                                                          &m_vel[i], &m_eff[i]);        

        hardware_interface::JointHandle jointEffortHandle(state_handle, &m_cmd[i]);

        m_jntStateInterface.registerHandle(state_handle);
        m_effortInterface.registerHandle(jointEffortHandle);
        m_velocityInterface.registerHandle(jointEffortHandle);
        m_positionInterface.registerHandle(jointEffortHandle);
    }


    registerInterface(&m_jntStateInterface);
    registerInterface(&m_effortInterface);
    registerInterface(&m_positionInterface);
    registerInterface(&m_velocityInterface);

    ServoController::Settings settings;
    settings.filename = "/dev/ttyACM0";
    m_servoController.Init(settings);

    reset();
    write();
}

void CarHardwareInterface::reset()
{
    for (unsigned int i = 0; i < m_jointNames.size(); i++)
    {
        m_pos[i] = m_vel[i] = m_eff[i] = m_cmd[i] = 0;
    }
}

void CarHardwareInterface::read()
{
    m_pos[0] = m_cmd[0];
    m_vel[1] = m_cmd[1];
}

void CarHardwareInterface::write()
{
    // Steering
    uint16_t effortSteering = (uint16_t)(5700 + 2000 * m_pos[0]);
    m_servoController.SendCommand(1, effortSteering);

    uint16_t effortWheels = (uint16_t)(6000 + 2000 * m_vel[1]);
    m_servoController.SendCommand(0, effortWheels);
}