/**
 * ServoController main class
 * Interfaces with Maestro 18 servo controller
 * 
 * Author: Chris
  * February, 2019
 * 
 * See "ServoController" section in Readme
 * 
 */

#ifndef SERVO_CONTROLLER_H_
#define SERVO_CONTROLLER_H_

#include <fstream>
#include <string>
#include <mutex>

namespace racer
{
class ServoController
{
  public:
    struct Settings
    {
        std::string filename = "/dev/ttyACM0";
    };

    enum ControllerState
    {
        READY = 0,
        NOT_READY = 1
    };

  public:
    ServoController(Settings &settings);
    ServoController();

    ~ServoController();

    void Init(Settings &m_settings);

    /**
     * We need this wrapper command for WriteRaw for 
     * the reconnect logic.
     */
    void SendCommand(uint8_t servoNo, uint16_t target);

  private:

    void WriteRaw( uint8_t servoNo, uint16_t target );

    std::mutex m_mutex;

    ControllerState m_state;
    Settings m_settings;
    std::ofstream m_tty;
};
} // namespace racer

#endif