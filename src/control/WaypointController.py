#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Image
import numpy as np
import cv_bridge
import geometry_msgs
from std_msgs.msg import Bool
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import Twist
import tf2_ros
import matplotlib.pyplot as plt
import cv_bridge
import cv2

bridge = cv_bridge.CvBridge()

class WaypointController(object):
    def __init__(self):
        self.autopilotEnabled = False
        self.apeSub = rospy.Subscriber(
            "/racer/autopilot_enable", Bool, self.autoPilotModeCallback)
        self.resetSub = rospy.Subscriber(
            "/racer/reset_odometry", Bool, self.resetCallback)
        self.cmdPub = rospy.Publisher(
            '/racer/car_controller/command', Twist, queue_size=1)
        self.stateSub = rospy.Subscriber(
            "/racer/odometry", Odometry, self.StateEstimationCallback, queue_size=1)

        self.currWaypoint = 0

        self.waypoints = [np.array([19.0,0.0]), np.array([19.0, -30.0]),np.array([0.0, 0.0])]        
        self.waypointDist = [19.0, 29.5, 20.0]
        
        self.stateEst = np.zeros(3)
        self.tfBuffer = tf2_ros.Buffer()
        self.mapPosListener = tf2_ros.TransformListener(self.tfBuffer)
        self.mapStateEst = np.zeros(3)

        self.lastPoint = np.zeros(2)
        self.firstPoint = True
        self.mapMode = True
        self.trajectory = []

    def resetCallback(self, msg):
        if(msg.data is True):
            self.currWaypoint = 0

    def StateEstimationCallback(self, msg):
        q = (msg.pose.pose.orientation.x,
             msg.pose.pose.orientation.y,
             msg.pose.pose.orientation.z,
             msg.pose.pose.orientation.w)
        euler = euler_from_quaternion(q)
        self.stateEst = np.array(
            [msg.pose.pose.position.x, msg.pose.pose.position.y, euler[2]])
        self.mapStateEst = self.stateEst

    def calcAngleBetween(self, vec1, vec2):
        det = vec1[0]*vec2[1] - vec1[1]*vec2[0]
        dot = np.dot(vec1, vec2)
        return np.arctan2(det, dot)

    def CalcWaypointDirection(self, estState, waypoint):
        lookDir = np.array([np.cos(estState[2]), np.sin(estState[2])])
        waypointDir = waypoint -estState[:2]
        angle = self.calcAngleBetween(waypointDir, lookDir)
        return angle

    def TranslateAngleToSteering(self, angle):
        steering = angle/0.43 # (angle/3.14159)*1.0
        return steering

    def Brake(self):
        cmdMsg = Twist()
        cmdMsg.angular.z = 0.0
        cmdMsg.linear.x = -0.1
        return cmdMsg

    def ThrottleStrategy(self, waypointIdx, estPos, angleDifference):
        dist = self.waypointDist[waypointIdx]
        estDist = np.linalg.norm(self.waypoints[waypointIdx] - estPos)        
        ratio = min( [(estDist)/(dist*0.75),1.0 ] )

        ratio2 = 1.0

        return ratio, ratio2       

    def Step(self):        
        
        msg = geometry_msgs.msg.Twist()

        cmdMsg = Twist()
        cmdMsg.angular.z = 0.0
        cmdMsg.linear.x = 0.0

        if(self.currWaypoint >= len(self.waypoints)):            
            self.cmdPub.publish(self.Brake())
            return

        estDistToWayponit = np.linalg.norm(self.waypoints[self.currWaypoint] - self.mapStateEst[:2])

        if(estDistToWayponit < 1.0):
            self.currWaypoint += 1
            return

        # Steering angle
        angleToWaypoint = self.CalcWaypointDirection(
            self.mapStateEst, self.waypoints[self.currWaypoint])

        # Constant throttle
        ratio1, ratio2 = self.ThrottleStrategy(self.currWaypoint, self.mapStateEst[:2], angleToWaypoint)
        print("Throttle Strategy: ", ratio1*ratio2)
        cmdMsg.linear.x = max(0.3*ratio1*ratio2,0.20)
     
        cmdMsg.angular.z = ratio2*self.TranslateAngleToSteering(angleToWaypoint)

        print("STATE: ", self.stateEst)
        print("WAYPOINT: ", self.waypoints[self.currWaypoint], self.currWaypoint)        
        print("Dist:", estDistToWayponit)
        print("Dir:", angleToWaypoint)
        print("CMD: ", cmdMsg.linear.x, cmdMsg.angular.z)

        if(self.autopilotEnabled):
            self.cmdPub.publish(cmdMsg) 

    def autoPilotModeCallback(self, data):
        self.autopilotEnabled = data.data

    def Run(self):
        rate = rospy.Rate(100)
        while not rospy.is_shutdown():
            self.Step()
            rate.sleep()

        if(self.mapMode == True):
            traj = np.array(self.trajectory)
            np.savetxt("/home/chris/trajectory.csv", traj)


if __name__ == '__main__':
    rospy.init_node("WaypointController")
    wpController = WaypointController()
    wpController.Run()

