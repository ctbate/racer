/**
 * Main file for key stroke publisher fore remote control over SSH
 *
 * rosrun racer telemetryler
 *
 * Author: Chris
 */

#include <ros/ros.h>
#include "Telemetry.hpp"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "telemetry");

  ros::NodeHandle nodeHandle;

  try
  {
    racer::Telemetry publisher(nodeHandle, racer::Telemetry::MODE_GAMEPAD);

    publisher.Init();

    publisher.Run();
  } catch(std::exception & e){
      std::cerr<< e.what()<<std::endl;
      ROS_ERROR("%s",e.what());
  }

  return 0;
}