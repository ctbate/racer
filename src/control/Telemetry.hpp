/**
 * Telemetry.hpp
 * Implements keyboard control using linux ncurses library
 * Publishes CarAcceleration/CarControl messages based on keystroke
 * 
 * February 2019
 * 
 * Author: Chris
 */
#ifndef KEYPAD_PUB_
#define KEYPAD_PUB_

#include <ros/ros.h>
#include <fstream>
#include <string>

#include "../hardware_interface/Gamepad.hpp"

namespace racer
{
class Telemetry
{
  public:
	enum ControlMode
	{
		MODE_KEYS = 0,
		MODE_GAMEPAD = 1
	};

  public:
	Telemetry(ros::NodeHandle &node, ControlMode mode = MODE_GAMEPAD);
	~Telemetry();

	void Init();

	void Run();

	bool HandleGamepad(const double &elapsedSec);

  private:
	ros::Publisher throttlePublisherLeft;
	ros::Publisher throttlePublisherRight;
	ros::Publisher steeringPublisher;
	ros::Publisher m_commandPublisher;
	ros::Publisher m_autopilotEnablePublisher;
	ros::NodeHandle &m_nodeHandle;
	ctbate::Gamepad m_gamepad;
	ControlMode m_controlMode;
	double m_maxVelocity;
	std::string m_jsFilename;
	void FillParam(const std::string &name, std::string &value, const std::string & def);
};
} // namespace racer

#endif