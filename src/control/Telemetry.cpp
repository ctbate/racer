/**
 * Telemetry.cpp
 * Implements keyboard control using linux ncurses library
 * Publishes CarAcceleration/CarControl messages based on keystroke
 *
 * February 2019
 *
 * Author: Chris
 */
#include "Telemetry.hpp"

#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>
#include <chrono>
#include <geometry_msgs/Twist.h>
#include <string>
#include <thread>

using namespace racer;
using namespace std;
using namespace ctbate;

const double throttleAccel = 20;  // 250 uSec duty change / sec
const double steeringAccel = 200; // 250 uSec duty change / sec
const double brakingAccel = 20;

/**
 * Constructor/Destructor
 */
Telemetry::Telemetry(ros::NodeHandle &node, Telemetry::ControlMode mode)
	: m_nodeHandle(node), m_controlMode(mode)
{
}

Telemetry::~Telemetry()
{
}

/**
 * Init
 * Advertises publishers to CarAcceleration and CarMessage
 */
void Telemetry::Init()
{
	try
	{
		m_commandPublisher =
			m_nodeHandle.advertise<geometry_msgs::Twist>("/racer/car_controller/command", 1);
		FillParam("/racer/js_file", m_jsFilename, "/dev/input/js0");
		m_gamepad.Start(m_jsFilename);
	}
	catch (std::exception &e)
	{
		std::cerr << e.what() << std::endl;
		ROS_ERROR("%s", e.what());
	}

	m_autopilotEnablePublisher = m_nodeHandle.advertise<std_msgs::Bool>("/racer/autopilot_enable", 1);

	m_nodeHandle.getParam("/racer/maxVelocity", m_maxVelocity);
	/**
   	* Wait for advertisement to propogate.
   	*/
	ros::Duration(1.0).sleep();
}

bool Telemetry::HandleGamepad(const double &elapsedSec)
{
	static Gamepad::GamepadEvent gEvent;
	static unsigned int count = 0;
	if (!m_gamepad.Read(gEvent))
	{
		try
		{
			m_gamepad.Start(m_jsFilename);
			
		}
		catch (std::exception &e)
		{
		}
		return false;
	}

	// Skip the first several events
	if (count < 10)
	{
		count++;
		return true;
	}

	bool enableAutopilot = false;

	static double velValue = 0.0;
	static double steerValue = 0.0;

	geometry_msgs::Twist msg;

	if (gEvent.type == Gamepad::BUTTON)
	{
		switch (gEvent.bid)
		{
		case Gamepad::B_BUTTON:
			break;
		case Gamepad::A_BUTTON:
			enableAutopilot = true;
		default:
			break;
		}
	}
	else
	{
		switch (gEvent.aid)
		{
		// Accelerate
		case Gamepad::RIGHT_TRIGGER:
			velValue = m_maxVelocity * gEvent.axisValue;
			break;
		// break
		case Gamepad::LEFT_TRIGGER:
			velValue = -m_maxVelocity * gEvent.axisValue;
			break;
		// Steer
		case Gamepad::LEFT_THUMB_X:
			steerValue = gEvent.axisValue;
			break;
		default:
			break;
		}

		msg.linear.x = velValue;
		msg.angular.z = steerValue;
		m_commandPublisher.publish(msg);

		// Disable autopilot for any controller input
		std_msgs::Bool enableMsg;
		enableMsg.data = false;
		m_autopilotEnablePublisher.publish(enableMsg);
	}

	if (enableAutopilot)
	{
		std_msgs::Bool enableMsg;
		enableMsg.data = true;
		m_autopilotEnablePublisher.publish(enableMsg);
	}

	return true;
}

/**
 * Run
 *
 * Runs an infinte loop, handling key presses
 * and publishing the required messages
 */
void Telemetry::Run()
{
	ros::AsyncSpinner(1);
	auto pollTime = ros::Rate(100);

	while (ros::ok())
	{
		try
		{
			HandleGamepad(0.05);
			pollTime.sleep();
		}
		catch (std::exception &e)
		{
			ROS_ERROR("%s", e.what());
			return;
		}
		ros::spinOnce();
	}
}

/**
 * Helper function for filling paramaters
 **/
void Telemetry::FillParam(const std::string &name, std::string &value, const std::string &def)
{
	if (!m_nodeHandle.hasParam(name))
	{
		value = def;
	}
	else
	{
		m_nodeHandle.getParam(name, value);
	}
}