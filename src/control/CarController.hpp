/**
 * An Ackerman-steered vehicle controller for rc cars, for use with ROS control
 *
 * Author: Christopher Bate March, 2019
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 * github.com/christopherbate/CarController
 * 
 *  
*/

#ifndef CAR_CONTROLLER
#define CAR_CONTROLLER

#include <ros/ros.h>
#include <hardware_interface/joint_command_interface.h>
#include <controller_interface/multi_interface_controller.h>
#include <geometry_msgs/Twist.h>

namespace racer
{
class CarController : public controller_interface::MultiInterfaceController<hardware_interface::PositionJointInterface, hardware_interface::EffortJointInterface>
{
  public:
	bool init(hardware_interface::RobotHW *robotHW, ros::NodeHandle &nodeHandle);

	void update(const ros::Time &time, const ros::Duration &period);

	void starting(const ros::Time &time);
	void stopping(const ros::Time &time);

	void openLoop();

	void cmdRecvCallback(const geometry_msgs::Twist::ConstPtr &msg);

	void FillParam(const std::string &name, std::string &value, const std::string &def);

  private:
	hardware_interface::JointHandle m_jhRearWheel;
	hardware_interface::JointHandle m_jhSteering;
	ros::Subscriber m_subCommand;
	double m_throttleGain;
	double m_steeringGain;
	double m_throttleSetPoint;
	double m_steeringSetPoint;
	double m_wheelSeperation;
	double m_wheelRadius;	
	double m_maxSteeringAngle;
	double m_pos[3];
	double m_heading;
	double m_headingVel;
	double m_maxVel;
	double m_vel[3];
};
} // namespace racer
#endif // !THROTTLE_CONTROLLER
