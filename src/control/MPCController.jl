#!/usr/bin/env julia
ENV["PYTHON"] = "/usr/bin/python2.7"
using JuMP
using Ipopt
using RobotOS
@rosimport nav_msgs.msg:Odometry
rostypegen(RobotOS)
using RobotOS.nav_msgs.msg

struct ControlsSolution
    throttle::Float64
    steering::Float64
    u::Array{Float64}
    z::Array{Float64}
    J::Array{Float64}
    ControlsSolution(throttle=0.0,steering=0.0,u=Float64[],z=Float64[],J=Float64[]) = new(throttle,steering,u,z,J)
end

function SolveForControls(xStart, yStart, yawStart, vStart, x_ref, y_ref, yaw_ref, soln::ControlsSolution)
    println("In controls function")   
    println("Optimizing")
        
    m = Model(with_optimizer(Ipopt.Optimizer, print_level = 3))
    print(m)
    dt = 0.05
    T = 100
    L_r = 0.14
    L = 0.30    
    @variable(m, x[1:(T + 1)], start = xStart)
    @variable(m, y[1:(T + 1)], start = yStart)
    @variable(m, yaw[1:(T + 1)], start = yawStart)
    @variable(m, v[1:(T + 1)], start = vStart)
    @variable(m, a[1:(T)])
    @variable(m, d_f[1:(T)])
    @NLexpression(m, bta[i = 1:T], atan((L_r / L) * tan(d_f[i])))
    for i in 1:T
        @NLconstraint(m, x[i + 1] == x[i] + dt * (v[i] * cos(yaw[i] + bta[i])))
        @NLconstraint(m, y[i + 1] == y[i] + dt * (v[i] * sin(yaw[i] + bta[i])))
        @NLconstraint(m, yaw[i + 1] == yaw[i] + dt * ((v[i] / L_r) * sin(bta[i])))
        @NLconstraint(m, v[i + 1] == v[i] + dt * a[i])
        @NLconstraint(m, a[i] <= 5.0 )
    end
    @NLobjective(m, Min, (x[T + 1] - x_ref)^2 + (y[T + 1] - y_ref)^2)
    
    @time optimize!(m)
    objective_value(m)
    
    soln.steering = value(d_f[1])
    soln.throttle = value(a[1])
end

function callback(msg::Odometry, lastSolveTime, soln::ControlsSolution)
    println("Received Odometry")
    print(msg)
    
    timeNow = get_rostime()
    timeNow = to_sec(timeNow)
    if(timeNow - lastSolveTime > 1.0)
        println("Solving MPC Problem")
        SolveForControls(0.0,0.0,0.0,0.0, 1.0, 1.0, 0.0, soln)
        lastSolveTime = timeNow
        print(soln.steering)
        println("")
        print(soln.throttle)
    end
end

function main()
    init_node("mpc_node")
    lastSolveTime = get_rostime()
    lastSolveTime = to_sec(lastSolveTime)
    soln = ControlsSolution()

    # solnPub = Publisher("/racer/mpc_solution", Float32MultiArray )

    sub = Subscriber{Odometry}("/racer/odometry", callback, (lastSolveTime, soln), queue_size = 10)    
    loop_rate = Rate(5.0)
    while ! is_shutdown()
        rossleep(loop_rate)
    end
end

if ! isinteractive()
    main()
else 
    soln = ControlsSolution(0.0,0.0,0.0,0.0,0.0)
    SolveForControls(0.0,0.0,0.0,0.0, 1.0, 1.0, 0.0, soln)
end