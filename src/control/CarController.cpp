/**
 * An Ackerman-steered vehicle controller for rc cars, for use with ROS control
 *
 * Author: Christopher Bate March, 2019
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE', which is part of this source code package.
 * github.com/christopherbate/CarController
 * 
*/

#include "CarController.hpp"
#include <pluginlib/class_list_macros.h>
#include <iostream>
#include <string>
#include <tf2/LinearMath/Quaternion.h>
#include <nav_msgs/Odometry.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf/tf.h>

using namespace racer;

bool CarController::init(hardware_interface::RobotHW *robotHW, ros::NodeHandle &nodeHandle)
{
    m_throttleGain = 1.0;
    m_steeringGain = 1.0;
    m_throttleSetPoint = 0.0;
    m_steeringSetPoint = 0.0;
    m_wheelRadius = 0.010;
    m_wheelSeperation = 0.050;
    m_pos[0] = m_pos[1] = m_pos[2] = 0.0;
    m_vel[0] = m_vel[1] = m_vel[2] = 0.0;
    m_heading = 0.0;
    m_headingVel = 0.0;
    m_maxSteeringAngle = 0.78;

    std::string rearWheelJoint;
    std::string steeringJoint;

    if (!nodeHandle.getParam("rear_wheel_joint", rearWheelJoint))
    {
        ROS_ERROR("Could not find left_rear_wheel_joint");
        return false;
    }

    if (!nodeHandle.getParam("steering_joint", steeringJoint))
    {
        ROS_ERROR("Could not find steering_hinge_joint");
        return false;
    }

    nodeHandle.getParam("velocityGain", m_throttleGain);
    nodeHandle.getParam("maxSteeringAngle", m_throttleGain);

    hardware_interface::PositionJointInterface *posInterface;
    hardware_interface::EffortJointInterface *effInterface;

    effInterface = robotHW->get<hardware_interface::EffortJointInterface>();
    posInterface = robotHW->get<hardware_interface::PositionJointInterface>();

    m_jhRearWheel = effInterface->getHandle(rearWheelJoint);
    m_jhSteering = posInterface->getHandle(steeringJoint);

    m_subCommand = nodeHandle.subscribe("/racer/car_controller/command", 1, &CarController::cmdRecvCallback, this);

    nodeHandle.getParam("/racer/maxVelocity", m_maxVel);

    return true;
}

void CarController::update(const ros::Time &time, const ros::Duration &period)
{

    m_jhSteering.setCommand(m_steeringSetPoint);

    double wheelVel = m_jhRearWheel.getVelocity();
    double cmd = m_throttleGain * (m_throttleSetPoint - wheelVel);

    m_jhRearWheel.setCommand(cmd);
}

void CarController::starting(const ros::Time &time)
{
    m_jhRearWheel.setCommand(0.0);
    m_jhSteering.setCommand(0.0);
}

void CarController::cmdRecvCallback(const geometry_msgs::Twist::ConstPtr &msg)
{
    m_steeringSetPoint = msg->angular.z;
    m_throttleSetPoint = msg->linear.x;
    if (m_throttleSetPoint < m_maxVel * -1.0)
    {
        m_throttleSetPoint = m_maxVel;
    }
    if (m_throttleSetPoint > m_maxVel)
    {
        m_throttleSetPoint = m_maxVel;
    }
    if (m_steeringSetPoint < -1.0)
    {
        m_steeringSetPoint = -1.0;
    }
    if (m_steeringSetPoint > 1.0)
    {
        m_steeringSetPoint = 1.0;
    }
}

void CarController::stopping(const ros::Time &time)
{
    m_jhRearWheel.setCommand(0.0);
    m_jhSteering.setCommand(0.0);
}

void CarController::openLoop()
{
}

PLUGINLIB_EXPORT_CLASS(racer::CarController, controller_interface::ControllerBase);