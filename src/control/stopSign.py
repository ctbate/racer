#!/usr/bin/env python3
import rospy
from sensor_msgs.msg import Image
import numpy as np
import cv_bridge
from std_msgs.msg import Bool
from geometry_msgs import Twist
import cv2
import imutils
import time

bridge = cv_bridge.CvBridge()
ifdistance = 180 
skipFrame = 15

def detect_haar(classifier, img):
	classifier = cv2.CascadeClassifier(classifier)
	gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

	stop_signs = classifier.detectMultiScale(gray, 1.02, 10)
	stop_singBool = np.any(stop_signs)
	for(x,y,w,h) in stop_signs:
		#cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
		#cv2.imshow("img",img)
		if(w>distance and h >distance):
			return stop_singBool
	
	#return stop_signs
def Brake(self):
	cmdMsg = Twist()
	cmdMsg.angular.z = 0.0
	cmdMsg.linear.x = -0.1
	return cmdMsg

def Go(self):
	cmdMsg = Twist()
	cmdMsg.angular.z = 0.0
	cmdMsg.linear.x = 0.2
	return cmdMsg

class stoptest:
	def __init__(self):
		self.cmdPub = rospy.Publisher('/racer/sign_detected', Bool, queue_size=1)
		#self.cmdPub = rospy.Publisher('/racer/sign_detected', Twist, queue_size=1)
		#self.imgSub = rospy.Subscriber("/camera/depth/image_rect_raw", Image, self.imgCallback)
		self.imgSub = rospy.Subscriber("/camera/color/image_raw",Image, self.imgCallback)
		self.counter = 0
	def imgCallback(self,data):
		data = bridge.imgmsg_to_cv2(data, desired_encoding="passthrough")
		if self.counter%skipFrame == 0:
			if detect_haar("/rosbags/HAAR_classifier.xml",data):
				print("Stop!")
				#msg = std_msgs.msg()
				#msg.stop = 1
				msg = Bool(True)
				self.cmdPub.publish(msg)
				#self.cmdPub.publish(self.Brake())
			else:
				print("Go")
				#msg = std_msgs.msg()
				#msg.stop = 0
				msg = Bool(False)
				self.cmdPub.publish(msg)
				#self.cmdPub.publish(self.Go())
		self.counter = self.counter + 1

if __name__=='__main__':
	ic = stoptest()
	rospy.init_node("stoptest")
	try:
		rospy.spin()
	except KeyboardInterrupt:
		print("Shutting down")
	cv2.destroyAllWindows()