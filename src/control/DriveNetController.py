#!/usr/bin/env python3
import rospy
from sensor_msgs.msg import Image
from std_msgs.msg import Bool
import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import transforms, models
import numpy as np
import cv_bridge
import geometry_msgs

class ConvNetColor(nn.Module):
    def __init__(self, latent_dim=50, out_dim=2):
        self.latent_dim = latent_dim
        super(ConvNetColor, self).__init__()
        self.layers = nn.Sequential(
            nn.Conv2d(3, 64, 3, stride=(2, 2)),
            nn.ReLU(True),
            nn.Conv2d(64, 128, 3, stride=(2, 2)),
            nn.ReLU(True),
            nn.Conv2d(128, 256, 3, stride=(2, 2))
        )

        self.fc1 = nn.Linear(386048, latent_dim)
        self.fc_output = nn.Linear(latent_dim, 2)

    def forward(self, x):
        x = self.layers(x)
        x = x.view(x.size(0), -1)
        x = F.relu(self.fc1(x))
        x = self.fc_output(x)
        return x

model_ft = models.resnet18(pretrained=True)
num_ftrs = model_ft.fc.in_features
model_ft.fc = nn.Sequential(nn.Linear(num_ftrs, 1), nn.Sigmoid())


class DriveNetController(object):
    def __init__(self):        
        self.autoPilotEnabled = False
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")        
        self.bridge = cv_bridge.CvBridge()
        self.cmdPub = rospy.Publisher('/racer/car_controller/command', geometry_msgs.msg.Twist, queue_size=1)
        self.model = model_ft # ConvNetColor()
        self.modelCheckpointFilename = rospy.get_param("/racer/drive_net/model_checkpoint")
        self.model.load_state_dict(torch.load(self.modelCheckpointFilename))
        self.normalize = transforms.Normalize(mean=[0.485, 0.485, 0.485], std=[0.229, 0.229, 0.229])
        self.model.to(self.device)
        self.model.eval()

        rospy.loginfo("DRIVENET: STARTED, device {}".format(str(self.device)))
        rospy.loginfo("DRIVENET: Model filename: {}".format(self.modelCheckpointFilename))        

    def ImageCallback(self, data):
        # Conduct inference
        if self.autoPilotEnabled:
            with torch.no_grad():
                data = np.asarray(self.bridge.imgmsg_to_cv2(
                    data, desired_encoding="passthrough"), dtype=np.float32)
                data = data.transpose((2, 0, 1))
                # data = np.swapaxes(data,0,2)
                # data = np.swapaxes(data,1,2)
                data = self.normalize(torch.from_numpy(data)).to(self.device)                  
                controls = self.model(data[None,:,:])                
                controls = controls.cpu().numpy()[0,:]
            # Publish controls to the command
            # controls[0] : predicted throttle
            # controls[1] : predicted steering
            msg = geometry_msgs.msg.Twist()        
            msg.linear.x = 0.2    
            msg.angular.z = (controls[0]-0.5)*1.0
            print(controls)
            self.cmdPub.publish(msg)


    def AP_ModeCallback(self, data):            
        self.autoPilotEnabled = data.data
        if(data == True):
            rospy.loginfo("AUTOPILOT ENABLED")
        
    def Run(self):        
        rospy.Subscriber("/camera/color/image_raw", Image, self.ImageCallback)
        rospy.Subscriber("/racer/autopilot_enable", Bool, self.AP_ModeCallback)
    
if __name__ == '__main__':
    rospy.init_node("driveNetController")
    controller = DriveNetController()

    controller.Run()

    rospy.spin()

