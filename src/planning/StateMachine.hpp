/**
 * StateMachine.hpp
 * Implements state machine used for midterm racing task
 *
 * Author: Cassie
 */
#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include <sensor_msgs/Image.h>
#include <ros/ros.h>
#include <string>

namespace racer
{
class StateMachine
{
  public:
	enum State
	{
		DRIVING = 0,
		TURNING = 1,
		ENDING = 2
	};

	struct Params
	{
		/** The distance for "distFront" below which 
         * the car will brake and halt. **/
		float stopDist;
		/** The P coef on the PID controller for 
         * turning 
         **/
		float turn_coefP;
		/** The I coef on the PID controller for turning **/
		float turn_coefI;
		/** The max effor to the rear motor ( scale 0 to 1) for the real car, 
         *  for simulation should be ~20 **/
		float maxEffortSpeed;
	};

  public:
	StateMachine(ros::NodeHandle &node);
	void Run();
	bool IsFinished();
	int Init();
	~StateMachine();

	void ImageCallback(const sensor_msgs::Image::ConstPtr &msg);

  private:
	void FillParam(const std::string &name, float &value, float defaultValue = 1.0);
	void FillParam(const std::string &name, bool &value, bool defaultValue = true);

	unsigned int numberTurns;

	float m_distLeft;
	float m_distFront;
	float m_distRight;

	Params m_params;

	State currentState;
	void MoveForward();

	/**
       * Simulation Publishers
       */
	ros::Publisher throttlePublisherLeft;
	ros::Publisher throttlePublisherRight;
	ros::Publisher steeringPublisher;

	/**
       * Real Actuator Publishers
       * TODO: Merge Real Actuator publishers with ROS Control system
       */
	ros::Publisher m_rlActuator;
	ros::Publisher m_steerEffortPub;
	ros::Publisher m_motorEffortPub;

	ros::Subscriber depthSubscriber;
	ros::NodeHandle &nodeHandle;
	unsigned int numImagesRecv;
};
} // namespace racer

#endif
