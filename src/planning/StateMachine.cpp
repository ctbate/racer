/**
 * StateMachine.cpp
 * Implements state machine used for midterm racing task
 *
 * Author: Cassie
 */

#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <std_msgs/Float64.h>
#include "StateMachine.hpp"

#define MAX_TURNS 2
#define NEAR_THRESHOLD 1
#define FAR_THRESHOLD 2

using namespace racer;
using namespace std;

// default constructor: set current state to driving to start
StateMachine::StateMachine(ros::NodeHandle &node) : nodeHandle(node), numImagesRecv(0)
{
	currentState = DRIVING;
	numberTurns = 0;

	FillParam("/racer/stopDist", m_params.stopDist, 500);
	FillParam("/racer/turn_coefP", m_params.turn_coefP, 0.5);
	FillParam("/racer/turn_coefI", m_params.turn_coefI, 0.5);
	FillParam("/racer/maxEffortSpeed", m_params.maxEffortSpeed, 0.2);

	ROS_INFO("Params: Stop %.2f, TurnPI ( %.2f, %.2f), Speed %.2f", m_params.stopDist, m_params.turn_coefP,
			 m_params.turn_coefI, m_params.maxEffortSpeed);

	m_distFront = m_distLeft = m_distRight = 0.0;
}

StateMachine::~StateMachine()
{
	// default deconstructor
}

int StateMachine::Init()
{
	try
	{
		throttlePublisherLeft =
			nodeHandle.advertise<std_msgs::Float64>("/racer/left_rear_wheel_velocity_controller/command", 10);
		throttlePublisherRight =
			nodeHandle.advertise<std_msgs::Float64>("/racer/right_rear_wheel_velocity_controller/command", 10);
		steeringPublisher =
			nodeHandle.advertise<std_msgs::Float64>("/racer/steering_hinge_position_controller/command", 10);

		depthSubscriber = nodeHandle.subscribe<sensor_msgs::Image>("/camera/depth/image_rect_raw", 10,
																   &StateMachine::ImageCallback, this);
	}
	catch (std::exception &e)
	{
		std::cerr << e.what() << std::endl;
		ROS_ERROR("%s", e.what());
		throw e;
	}

	ROS_INFO("StateMachine created!");

	return 0;
}

void StateMachine::ImageCallback(const sensor_msgs::Image::ConstPtr &msg)
{
	// numImagesRecv++;
	// // Simulation gives FC32 encoding,
	// // Realsense gives 16UC1 encoding
	const unsigned int senseRowStart = 30;
	const unsigned int senseRowEnd = 30;
	const unsigned int leftLimit = 25;
	const unsigned int rightLimit = 25;
	const unsigned int imgWidth = msg->width;
	// /** UNCOMMENT FOR REAL CAR **/
	_Float32(*msgMatrix)[imgWidth] = (_Float32(*)[imgWidth])msg->data.data();
	// // uint16_t(*msgMatrix)[imgWidth] = (uint16_t(*)[imgWidth])msg->data.data();
	m_distLeft = 0.0;
	for (unsigned int i = 0; i < leftLimit; i++)
	{
		m_distLeft += (float)msgMatrix[senseRow][i];
	}
	m_distLeft = (m_distLeft) / leftLimit;
	if (isnan(m_distLeft))
		m_distLeft = 0.0;

	m_distFront = 0.0;
	for (unsigned int i = leftLimit; i < imgWidth - rightLimit; i++)
	{
		m_distFront += (float)msgMatrix[senseRow][i];
	}
	m_distFront = (m_distFront) / (imgWidth - leftLimit - rightLimit);

	m_distRight = 0.0;
	for (unsigned int i = imgWidth - rightLimit; i < imgWidth; i++)
	{
		m_distRight += (float)msgMatrix[senseRow][i];
	}
	m_distRight = (m_distRight) / rightLimit;
	if (isnan(m_distRight))
		m_distRight = 0.0;
}

// Run the action of the current state
void StateMachine::Run()
{
	switch (currentState)
	{
	case DRIVING:
		MoveForward();
		break;
	default:
		break;
	}
}

void StateMachine::MoveForward()
{
	// TODO: determine best location to distance robot from far wall in hallway
	// = false;
	// drive forward
	std_msgs::Float64 speed;
	speed.data = 1.0;
	std_msgs::Float64 angle;

	float p_value;

	// Speed control
	if (m_distFront < 3)
	{
		speed.data = 0.5;
		if (m_distFront < 1)
		{
			speed.data = 0.0;
		}
	}

	// steering
	float error = m_distRight - m_distLeft;

	p_value = error;

	if (p_value > 1.0)
	{
		p_value = 1.0;
	}
	else if (p_value < -1.0)
	{
		p_value = -1.0;
	}
	angle.data = -1.0*p_value; //* m_params.turn_coefP;

	cout << angle.data << endl;

	speed.data *= m_params.maxEffortSpeed;
	throttlePublisherLeft.publish(speed);
	throttlePublisherRight.publish(speed);
	steeringPublisher.publish(angle);
}

/**
 * Helper function for filling paramaters
 **/
void StateMachine::FillParam(const std::string &name, float &value, float defaultValue)
{
	if (!nodeHandle.hasParam(name))
	{
		value = defaultValue;
	}
	else
	{
		nodeHandle.getParam(name, value);
	}
}

/**
 * Helper function for filling paramaters
 **/
void StateMachine::FillParam(const std::string &name, bool &value, bool defaultValue)
{
	if (!nodeHandle.hasParam(name))
	{
		value = defaultValue;
	}
	else
	{
		nodeHandle.getParam(name, value);
	}
}