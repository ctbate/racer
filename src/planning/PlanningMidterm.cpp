/**
 * planningMidterm
 * rosrun racer planningMidterm
 *
 * An simple planner that interacts with the ROS control nodes and output
 * control commands as a function of the received sensor input
 * Uses state machine to complete course
 *
 * Sensory input can be published from an actual sensor or simulated sensors.
 *
 * Based on planningExample.cpp
 *
 * Author: Cassie
 */

#include <ros/ros.h>
#include "StateMachine.hpp"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "planningMidterm");
  ros::NodeHandle nodeHandle;

  racer::StateMachine machine(nodeHandle);

  // Init throws an error on failure, so just do try/catch
  try{
    machine.Init();
  }
  catch(std::exception &e)
  {
    ROS_ERROR("StateMachine failed to start!");
    return -1;
  }

  while (ros::ok())
  {
    machine.Run();
    ros::spinOnce();
    ros::Duration(0.01).sleep();
  }
  return 0;
}
