#ifndef INERTIAL_HPP_
#define INERTIAL_HPP_

#include <ros/ros.h>
#include <sensor_msgs/Imu.h>

namespace racer{
class Inertial
{
public:
    Inertial(ros::NodeHandle &nodeHandle);
    ~Inertial();

    void subAccel( const sensor_msgs::Imu::ConstPtr & msg);
    void subGyro( const sensor_msgs::Imu::ConstPtr & msg);

    void Init();
    void Run();

private:
    ros::Subscriber m_accelSub;
    ros::Subscriber m_gyroSub;
    ros::NodeHandle &m_nodeHandle;
    double m_state[6];
    double m_accelBias[3];
    double m_avgBias[3];
};
}


#endif // !INERTIAL_HPP_


