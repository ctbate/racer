/**
 * Node for inertial measurements
*/
#include <ros/ros.h>
#include "Inertial.hpp"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "inertialNode");
  ros::NodeHandle nodeHandle;

  racer::Inertial inertialNode(nodeHandle);

  // Init throws an error on failure, so just do try/catch
  try{
    inertialNode.Init();
  }
  catch(std::exception &e)
  {
    ROS_ERROR("InertialNode failed to start!");
    return -1;
  }

  inertialNode.Run();
  
  return 0;
}
