#include <chrono>
#include <iostream>
#include "Inertial.hpp"
using namespace racer;

Inertial::Inertial(ros::NodeHandle &nodeHandle) : m_nodeHandle(nodeHandle)
{
  for (int i = 0; i < 6; i++)
  {
    m_state[i] = 0.0;
  }
  for (int i = 0; i < 3; i++)
  {
    m_accelBias[i] = 0.0;
  }

  m_accelSub = m_nodeHandle.subscribe("/camera/accel/sample", 10, &Inertial::subAccel, this);
  m_gyroSub = m_nodeHandle.subscribe("/camera/gyro/sample", 10, &Inertial::subGyro, this);
}

Inertial::~Inertial()
{
}

void Inertial::Init()
{
  ROS_INFO("Calibrating Inertials.");
}

void Inertial::Run()
{
  ros::spin();
}

void Inertial::subAccel(const sensor_msgs::Imu::ConstPtr &msg)
{
  static unsigned long measurementCnt = 0;
  static std::chrono::steady_clock::time_point lastTime = std::chrono::steady_clock::now();

  auto now = std::chrono::steady_clock::now();
  float elapsed = std::chrono::duration_cast<std::chrono::microseconds>(now - lastTime).count();

  if (measurementCnt > 500)
  {
    m_state[0] += m_state[3] * elapsed * 1e-6;
    m_state[1] += m_state[4] * elapsed * 1e-6;
    m_state[2] += m_state[5] * elapsed * 1e-6;
    m_state[3] += (msg->linear_acceleration.x - m_accelBias[0]) * elapsed * 1e-6 *0.5;
    m_state[4] += (msg->linear_acceleration.y - m_accelBias[1]) * elapsed * 1e-6 *0.5;
    m_state[5] += (msg->linear_acceleration.z - m_accelBias[2]) * elapsed * 1e-6 *0.5;

    if (measurementCnt % 500 == 0)
    {
      std::cout << "ELAPSED " << elapsed << " " << elapsed * 1e-6 << std::endl;
      std::cout << "POS: " << m_state[0] << ", " << m_state[1] << ", " << m_state[2] << std::endl;
      std::cout << "VEL: " << m_state[3] << ", " << m_state[4] << ", " << m_state[5] << std::endl;
    }
  }
  else
  {
    m_state[3] += (msg->linear_acceleration.x);
    m_state[4] += (msg->linear_acceleration.y);
    m_state[5] += (msg->linear_acceleration.z);

    if (measurementCnt == 500)
    {
      ROS_INFO("Calibration Complete");
      std::cout << "ELAPSED " << elapsed << " " << elapsed * 1e-6 << std::endl;
      std::cout << "POS: " << m_state[0] << ", " << m_state[1] << ", " << m_state[2] << std::endl;
      std::cout << "VEL: " << m_state[3] << ", " << m_state[4] << ", " << m_state[5] << std::endl;
      m_accelBias[0] = m_state[3] / 500;
      m_accelBias[1] = m_state[4] / 500;
      m_accelBias[2] = m_state[5] / 500;
      for (int i = 0; i < 6; i++)
      {
        m_state[i] = 0.0;
      }
      for (int i = 0; i < 3; i++)
      {
        std::cout << "BIAS " << m_accelBias[i] << std::endl;
      }
    } else {
      std::cout << msg->linear_acceleration.x << " ";
    }
  }

  lastTime = now;
  measurementCnt++;
};

void Inertial::subGyro(const sensor_msgs::Imu::ConstPtr &msg){};