# Team9 Autonomous RC Car

# Basic Tasks 

Ensure you sourced `devel/setup.bash` in your shell.

## Host Pre-requisites

```
[must add ros repos to apt]

ros-melodic-ros-base
ros-melodic-ros-control
ros-melodic-effort-controllers
ros-melodic-joint-state-controller
ros-melodic-robot
ros-melodic-ros-controllers
ros-melodic-tf2
ros-melodic-tf2-geometry-msgs
ros-melodic-cv-bridge
ros-melodic-image-transport
libhidapi-libusb0 
libturbojpeg

intel realsense library with kernel modifications
realsense-ros package
For the computer connected to realsense, pololu - need to apply udev rules for pololu and realsense
```

## Start Simulator

```
roslaunch racer simulator.launch
```

Can observe data in RVIZ, move around with controller, etc.

## Manual Drive

```
roslaunch racer ManualDrive.launch
```


# Misc Tasks

## Testing the Servo Controller (Pololu Mini Maestro 18)

Gere [here](https://www.pololu.com/docs/0J41) to get the Maestro Servo Controller Linux Software Package. 

> You will need the following packages on Ubuntu (at least):
> 1. libmono-system-windows.forms4.0-cil
> That should install associated packages you need.

Install the udev rules file:

> 1. `cd (where you untared the pololu package)`
> 2. `cp 99-pololu.rules /etc/udev/rules.d`
> 3. `sudo udevadm control --reload-rules`

Plug in Pololu via mini-USB to your computer. 

Launch the program:
`./MaestroControlCenter`

> Setting the Channel Settings
> To configure the servo settings for channel 0 to be the motor, channel 1 to be the servo, do the following:
> Channel 0 and 1: min 992, max 2000, 8bit neutral 1500, 8 bit range 476.25

To test (without motor connected):

> ESC has a power switch. Turn it off.
> Plug in small wires from ESC to the Maestro channel 0
> Plug in Maestro over USB and start Control Center
> Connector battery to ESC
> Turn on ESC
> In the control center, setting the target to 1500 on channel 0 should cause no blinking LED on ESC
> Setting to 1550 should cause it to start blinking. This is forward mode.
> 1500 blinking stops
> Setting to 1450 should cause it to start blinking again (reverse/brake, see below)

Test (with motor):
> Turn off ESC
> Set channel 0 to 1500
> plug in motor to ESC (BLACK to BLUE, YELLOW to RED)
> Turn on ESC. It will beep.
> 1550 wheels turn forward

## Basic Information About Motor Control

#### How Forward / Reverse / Braking Works

1. Forward: Setting PWM to over 1.5 msec (1500) will cause wheels to spin forward. 1550 is about minimal speed forward, 2000 is max.
2. Brake: Setting PWM to under 1.5 msec (1500) will cause brake IF you are moving from > 1500 to 1400.
3. Reverse: Setting PWM to under 1.5 msec (1500) after braking will cause reverse. So to reverse the from neutral, you must send (1500 -> 1400 -> 1500 -> 1400). Reverse has a lower max speed than forward.




