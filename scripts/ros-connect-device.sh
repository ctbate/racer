#!/bin/bash
if [ -z $1 ]; then
    echo "Usge: ./ros-connect-device.sh [rock64 ip] [your ip]"
    exit
fi
export ROS_MASTER_URI=http://$1:11311
export ROS_IP=$2
export ROS_HOSTNAME=$2

# Other commands
rostopic list