#!/bin/bash
# Builds Docker app container base image
# usage: ./scripts/buildAppContainer.sh car_app_container

if [ -z $1 ]; then
    echo "You must supply an image name and tag for this deploy"
    exit
fi

echo "Building image $1..."

docker build . -t $1 --file scripts/containers/app-container/Dockerfile

read -p "Push image to Docker repo $0?(y/n): " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    docker push $1
fi

