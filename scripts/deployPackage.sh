#!/bin/bash
# Copies the code directory, less the deploy and docs folder, to a raspberry pi target
# Usage: ./scripts/deployPackage deployment_1 192.168.1.25

if [ -z $1 ]; then
    echo "You must supply a name for this deployment."
    exit
fi

if [ -z $2 ]; then
    echo "You must supply an IP"
    exit
fi

tar --exclude="./deploy" --exclude="./docs"  -czf deploy/$1.tar.gz ./

# Deploy the file
scp ./deploy/$1.tar.gz rock64@$2:~/

# Execute unpack, and build
ssh rock64@$2 "mkdir $1 && tar -C $1 -xzf $1.tar.gz && cd $1 && ./scripts/dockerDeploy.sh racer:latest"

