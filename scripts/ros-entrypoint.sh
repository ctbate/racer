#!/bin/bash

# Note this a script to be run inside the docker container, not 
# for local dev
. /opt/ros/melodic/setup.bash
. /home/ros/catkin_ws/devel/setup.bash

exec "$@"
