#!/bin/bash
# Sends a Set Target command to a Pololu Maestro servo controller
# via its virtual serial port.
# Usage: maestro-set-target.sh DEVICE CHANNEL TARGET
# example: bash maestro-set-target.sh /dev/ttyACM0 0 6000
DEVICE=$1
CHANNEL=$2
TARGET=$3

byte() {
  printf "\\x$(printf "%x" $1)"
}

stty raw -F $DEVICE

{
  byte 0x84
  byte $CHANNEL
  byte $((TARGET & 0x7F))
  byte $((TARGET >> 7 & 0x7F))
} > $DEVICE