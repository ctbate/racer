#!/bin/bash
# Builds Docker image with the code. 
# usage: ./scripts/dockerDeploy.sh t9car

if [ -z $1 ]; then
    echo "You must supply an image name and tag for this deploy"
    exit
fi

echo "Building image $1..."

docker build . -t $1 --file scripts/Dockerfile

read -p "Push image to Docker repo $0?(y/n): " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    docker push $1
fi

