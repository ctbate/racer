<?xml version="1.0"?>
<robot name="racer" 
  xmlns:xacro="http://www.ros.org/wiki/xacro">

  <!-- Gazebo references -->

  <gazebo reference="chassis">
    <mu1 value="0.0"/>
    <mu2 value="0.0"/>
    <kp value="10000000.0" />
    <kd value="1.0" />
    <material>Gazebo/Red</material>
  </gazebo>

  <gazebo reference="left_rear_wheel">
    <mu1 value="2.0"/>
    <mu2 value="2.0"/>
    <kp value="10000000.0" />
    <kd value="1.0" />
    <fdir1 value="1 0 0"/>
    <material>Gazebo/Black</material>
  </gazebo>

  <gazebo reference="right_rear_wheel">
    <mu1 value="2.0"/>
    <mu2 value="2.0"/>
    <kp value="10000000.0" />
    <kd value="1.0" />
    <fdir1 value="1 0 0"/>
    <material>Gazebo/Black</material>
  </gazebo>

  <gazebo reference="front_wheel">
    <mu1 value="2.0"/>
    <mu2 value="2.0"/>
    <kp value="10000000.0" />
    <kd value="1.0" />
    <fdir1 value="0 0 1"/>
    <material>Gazebo/Black</material>
  </gazebo>

  <!-- Gazebo plugins -->

  <gazebo>
    <plugin name="gazebo_ros_control" filename="libgazebo_ros_control.so">
      <robotNamespace>/racer</robotNamespace>
    </plugin>
  </gazebo>

  <gazebo reference="base_link">
    <gravity>true</gravity>
    <sensor name="imu_sensor" type="imu">
      <always_on>true</always_on>
      <update_rate>100</update_rate>
      <visualize>true</visualize>
      <topic>__default_topic__</topic>
      <plugin filename="libgazebo_ros_imu_sensor.so" name="imu_plugin">
        <topicName>/camera/imu</topicName>
        <bodyName>base_link</bodyName>
        <updateRateHZ>10.0</updateRateHZ>
        <gaussianNoise>0.1</gaussianNoise>
        <xyzOffset>0 0 0</xyzOffset>
        <rpyOffset>0 0 0</rpyOffset>
        <frameName>base_link</frameName>
      </plugin>
      <pose>0 0 0 0 0 0</pose>
    </sensor>
  </gazebo>

  <!-- <gazebo>
    <plugin name="imu_plugin" filename="libgazebo_ros_imu.so">
      <alwaysOn>true</alwaysOn>
      <bodyName>base_link</bodyName>
      <topicName>/camera/imu</topicName>
      <serviceName>imu_service</serviceName>
      <gaussianNoise>0.0</gaussianNoise>
      <updateRate>20.0</updateRate>
    </plugin>
  </gazebo> -->

  <!-- real sense camera -->
  <xacro:unless value="$(optenv DISABLE_GAZEBO_CAMERA false)">
    <gazebo reference="camera_link">
      <sensor type="depth" name="realsense">
        <update_rate>30.0</update_rate>
        <!-- math.atan(320 / 687.8065795898438) * 2 -->
        <camera name="realsense_camera">
          <horizontal_fov>1.0</horizontal_fov>
          <image>
            <format>R8G8B8</format>
            <width>212</width>
            <height>120</height>
          </image>
          <clip>
            <near>0.05</near>
            <far>100</far>
          </clip>
        </camera>
        <plugin name="camera_plugin" filename="libgazebo_ros_openni_kinect.so">
          <baseline>0.2</baseline>
          <alwaysOn>true</alwaysOn>
          <!-- Keep this zero, update_rate in the parent <sensor> tag
            will control the frame rate. -->
          <updateRate>0.0</updateRate>
          <cameraName>camera</cameraName>
          <imageTopicName>/camera/rgb/image_raw</imageTopicName>
          <cameraInfoTopicName>/camera/rgb/camera_info</cameraInfoTopicName>
          <depthImageTopicName>/camera/depth/image_rect_raw</depthImageTopicName>
          <depthImageInfoTopicName>/camera/depth/camera_info</depthImageInfoTopicName>
          <pointCloudTopicName>/camera/depth/points</pointCloudTopicName>
          <frameName>camera_link_optical</frameName>
          <pointCloudCutoff>0.05</pointCloudCutoff>
          <distortionK1>0</distortionK1>
          <distortionK2>0</distortionK2>
          <distortionK3>0</distortionK3>
          <distortionT1>0</distortionT1>
          <distortionT2>0</distortionT2>
          <CxPrime>0</CxPrime>
          <Cx>0</Cx>
          <Cy>0</Cy>
          <focalLength>0</focalLength>
          <hackBaseline>0</hackBaseline>
        </plugin>
      </sensor>
    </gazebo>
  </xacro:unless>
</robot>
