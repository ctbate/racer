#!/bin/bash
IP_ADDR=`ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1  -d'/'`

echo "Ip addr is: $IP_ADDR"

export ROS_IP=$IP_ADDR
export ROS_HOSTNAME=$IP_ADDR
export ROS_MASTER_URI=http://$IP_ADDR:11311
