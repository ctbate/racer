#!/bin/bash

LOCAL_IP=`ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1  -d'/'`

echo "Your IP is $LOCAL_IP"
echo Enter the IP address of the Jetson Nano:

read NANO_ADDR

export ROS_IP=$LOCAL_IP
export ROS_HOSTNAME=$LOCAL_IP
export ROS_MASTER_URI=http://$NANO_ADDR:11311